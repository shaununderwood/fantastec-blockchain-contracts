import FantastecNFT from 0xFantastecNFT

pub fun main(): UInt64 {
  return FantastecNFT.totalSupply
}
