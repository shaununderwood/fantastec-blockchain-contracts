

import * as path from 'path';
import * as t from "@onflow/types";
import {
  init,
  sendTransaction,
  deployContractByName,
  getTransactionCode,
  // emulator,
  getContractAddress,
  getAccountAddress,
  getScriptCode,
  executeScript,
} from "flow-js-testing/dist";
import { TransactionEvent, TransactionResult, Card, CardCollection } from "../../shared/interfaces";
import { card1, card2, correctCardData, objectToKeyValueArray, correctCardCollectionData } from '../../shared/sharedAssets';

import emulator from "../../shared/emulatorWithNoLimit";

const basePath = path.resolve(__dirname, "../../FantastecNFT");
const port = 8080;
const internalEmulator = true; // TODO make env

const POSITION_NAME = 0;
const POSITION_LEVEL = 1;
const POSITION_CARD_TYPE = 2;
const POSITION_ASPECT_RATIO = 3;
const POSITION_METADATA = 4;
const POSITION_MEDIADATA = 5;
const POSITION_COLLECTIONID = 6;
const VALUE = 0;

beforeAll(() => {
  init(basePath, port);
});

describe("mint NFT", () => {
  let Alice;
  let signers;
  let contractAccount;

  beforeEach(async () => {
    if (internalEmulator) {
      await emulator.start(port, false);

      contractAccount = await getAccountAddress("Alice");

      // load contracts
      let basePath = path.resolve(__dirname, "../../standards");
      init(basePath, port);
      await deployContractByName({ to: contractAccount, name: "NonFungibleToken" });

      basePath = path.resolve(__dirname, "../../FantastecSwapData");
      init(basePath, port);
      await deployContractByName({ to: contractAccount, name: "FantastecSwapData" });

      basePath = path.resolve(__dirname, "../../FantastecNFT");
      init(basePath, port);
      const addressMap = { 
        NonFungibleToken: contractAccount,
        FantastecSwapData: contractAccount,
      };
      await deployContractByName({ to: contractAccount, name: "FantastecNFT", addressMap });
    }

    // set main account
    Alice = await getAccountAddress("Alice");

    // Set transaction signers
    signers = [Alice];
  });

  // Stop emulator, so it could be restarted
  afterEach(async () => {
    if (internalEmulator) return emulator.stop();
  });

  it("starts with totalSupply set to 0", async () => {
    const name = "getTotalSupply";
    const code = await getScriptCode({ name });
    await expect(executeScript({ code })).resolves.toBe(0);  
  });

  it.todo("has created a minter and saved it to storage");
  it.todo("refuses to mint when cardId is not in the DataContract");
  it.todo("refuses to mint when the Card isDeactivated");
  it.todo("refuses to mint when the CardCollection isDeactivated"); // Question: if collection is deactivated, all cards should be automatically deactivated also
  it.todo("refuses to mint when Card's minted volume is reached");
  it.todo("refuses to mint when Card is locked");
  it.todo("refuses to mint when the mintNumber already exists");

});
