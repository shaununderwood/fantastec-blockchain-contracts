import FantastecSwapNFT from 0x03
import NonFungibleToken from 0x02

// This transaction configures a user's account
// to use the NFT contract by creating a new empty collection,
// storing it in their account storage, and publishing a capability
transaction {
  prepare(signer: AuthAccount) {
    // if the account doesn't already have a regular FantastecSwapNFT collection
    if signer.borrow<&FantastecSwapNFT.Collection>(from: FantastecSwapNFT.CollectionStoragePath) == nil {

      // create a new empty collection
      let collection <- FantastecSwapNFT.createEmptyCollection()
        
      // save it to the account
      signer.save(<-collection, to: FantastecSwapNFT.CollectionStoragePath)

      // create a public capability for the collection
      signer.link<&FantastecSwapNFT.Collection{NonFungibleToken.CollectionPublic, FantastecSwapNFT.FantastecSwapNFTCollectionPublic}>(FantastecSwapNFT.CollectionPublicPath, target: FantastecSwapNFT.CollectionStoragePath)
    }
  }
}
