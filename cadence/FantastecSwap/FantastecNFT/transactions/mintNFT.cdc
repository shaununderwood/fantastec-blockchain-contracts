import NonFungibleToken from 0x02
import FantastecSwapNFT from 0x03

// This transction uses the NFTMinter resource to mint new NFTs (different serial numbers) with the same momentid.
//
// It must be run with the account that has the minter resource
// stored at path /storage/NFTMinter.

transaction(recipient: Address, cardId: UInt64, mintNumber: UInt64) {
    
  // local variable for storing the minter reference
  let minter: &FantastecSwapNFT.NFTMinter

  prepare(signer: AuthAccount) {

      // borrow a reference to the NFTMinter resource in storage
      self.minter = signer.borrow<&FantastecSwapNFT.NFTMinter>(from: FantastecSwapNFT.MinterStoragePath)
          ?? panic("Could not borrow a reference to the NFT minter")
  }

  execute {
      // get the public account object for the recipient
      let recipient = getAccount(recipient)

      // borrow the recipient's public NFT collection reference
      let receiver = recipient
          .getCapability(FantastecSwapNFT.CollectionPublicPath)
          .borrow<&{NonFungibleToken.CollectionPublic}>()
          ?? panic("Could not get receiver reference to the NFT Collection")

      // mint the NFT and deposit it to the recipient's collection
      self.minter.mintNFT(recipient: receiver, cardId: cardId, mintNumber: mintNumber)
  }
}
