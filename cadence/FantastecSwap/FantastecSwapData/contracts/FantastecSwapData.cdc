/**

# Contract: FantastecSwapData

## Description

The purpose of this contract is to provide a central location to hold and maintain metadata about FantastecSwap's Cards and Collections.

Collections represent a themed set of Cards, as indicated on their attributes.
Collections have 0 or more Cards associated with them.

Collections

Cards represent an individual item or moment of interest - a digital card of a player or stadium, a video moment, a VR scene, or access to other resources.

An NFT will be minted against individual Card.



 */
pub contract FantastecSwapData {

  /** EVENTS **/
  // Contract Events
  pub event ContractInitialized()

  // Card Events
  pub event CardCreated(item: FantastecSwapData.CardData)
  pub event CardUpdated(item: FantastecSwapData.CardData)
  pub event CardDeactivated(id: UInt64)
  pub event AddedEditionMintVolume(item: FantastecSwapData.CardData)

  // CardCollection Events
  pub event CardCollectionCreated(item: FantastecSwapData.CardCollectionData)
  pub event CardCollectionUpdated(item: FantastecSwapData.CardCollectionData)
  pub event CardCollectionDeactivated(id: UInt64)

  /** CONTRACT LEVEL PROPERTIES **/
  access(self) var cardCollectionData: {UInt64: CardCollectionData}
  access(self) var nextCardCollectionId: UInt64
  access(self) var cardData: {UInt64: CardData}
  access(self) var nextCardId: UInt64

  /** CONTRACT LEVEL RESOURCES */
  pub let AdminStoragePath: StoragePath

  pub struct IPORoyalty {
    // TODO might address instead by a capability to deposit FUSD into an account?
    pub let address: Address;
    pub let percentage: UFix64;
    init(
      address: Address, 
      percentage: UFix64,
    ){
      pre {
        percentage <= 100.0: "percentage cannot be higher than 100"
      }
      self.address = address;
      self.percentage = percentage;
    }
  }

  /** CONTRACT LEVEL STRUCTS */
  pub struct CardCollectionData {
    pub var id: UInt64
    pub var appId: String
    pub var title: String
    pub var shortTitle: String
    pub var description: String
    pub var level: String
    pub var metadata: {String: String}
    pub var marketplaceFee: UFix64
    pub var ipos: [IPORoyalty]

    // when isDeactivated is true the collection is considered soft deleted
    // eg if a mistake was made on the collection isDeactivated will signify
    // never to use this collection under most circumstances
    // * Collection marked as {isDeactivated:true} and cannot be accessed through "getAll"-like functions
    // * but can be referenced through "getById"-like function
    // * no actions can be performed on the collection nor it’s cards
    pub var isDeactivated: Bool

    pub fun deactivate(){
      self.isDeactivated = true
    }

    pub fun save(){
      FantastecSwapData.cardCollectionData[self.id] = self
    }

    init(
      appId: String, 
      title: String, 
      shortTitle: String, 
      description: String, 
      level: String,
      metadata: {String: String},
      marketplaceFee: UFix64,
      id: UInt64?, // if nil, nextCardCollectionId is used
      ){
      pre {
        appId.length != 0: "appId cannot be empty"
        title.length != 0: "title cannot be empty"
        shortTitle.length != 0: "shortTitle cannot be empty"
        description.length != 0: "description cannot be empty"
        level.length != 0: "level cannot be empty"
        marketplaceFee <= 100.0: "marketplaceFee cannot be more than 100"
      }

      self.appId = appId
      self.title = title
      self.shortTitle = shortTitle
      self.description = description
      self.level = level
      self.marketplaceFee = marketplaceFee
      if (metadata != nil){
        self.metadata = metadata;
      } else {
        self.metadata = {}
      }

      // add a default IPO
      var ipoAddress: Address = Address(0x01)
      var percentage: UFix64 = 100.0
      var ipoFantastec = IPORoyalty(address: ipoAddress, percentage: percentage);
      self.ipos = [ipoFantastec]

      // set the id
      if (id == nil){
        self.id = FantastecSwapData.nextCardCollectionId
        FantastecSwapData.nextCardCollectionId =  FantastecSwapData.nextCardCollectionId + (1 as UInt64)
      } else {
        self.id = id!
      }

      // set locks
      self.isDeactivated = false
    }
  }

  pub struct CardData {
    pub var id: UInt64                  // a unique sequential number
    pub var name: String                // Card name
    pub var level: String               // Level, one of 15 different levels
    pub var cardType: String            // Currently either [Player, Lineup]??
    pub var aspectRatio: String         // ??
    pub var metadata: {String: String}  // a data structure determined by cardType
    // pub var mediaData: [MediaType]   // TODO needs to change to MediaType
    pub var mediaData: String           // All media for an NFT is held in a mediaData array
                                        // An NFT should have at least 1 mediaData entry being an image representing the NFT
    pub var cardCollectionId: UInt64    // The id of the collection to which this card belongs

    // when isDeactivated is true the card is considered soft deleted
    // eg if a mistake was made on the card isDeactivated will signify
    // never to use this card under most circumstances
    // * Card is marked as {isDeactivated:true} and cannot be accessed through "getAll"-like functions
    // * but can be referenced through "getById"-like function
    // * no actions can be performed on the card
    pub var isDeactivated: Bool

    // The desired number of NFTs to mint per edition.
    // Admin may add a new entry to editionMintVolume, but not change any entries already present.
    // All other users have READONLY access to this property.
    // For example, {1:70} means 1st edition, 70 cards minted.
    // TODO: function to implement this
    pub var editionMintVolume: {UInt64: UInt64}

    init(
      name: String, 
      level: String, 
      cardType: String,
      aspectRatio: String,
      metadata: {String: String},
      mediaData: String, // TODO [MediaType],
      cardCollectionId: UInt64,
      id: UInt64?,
    ){
      pre {
        name.length > 0: "name cannot be empty"
        level.length > 0: "level cannot be empty"
        cardType.length > 0: "cardType cannot be empty"
        aspectRatio.length > 0: "aspectRatio cannot be empty"
        mediaData.length > 0: "mediaData cannot be empty"
        cardCollectionId > (0 as UInt64): "cardCollectionId must have a value greater than 1"
        FantastecSwapData.cardCollectionData[cardCollectionId] != nil: "cannot create cardData when cardCollectionId does not exist"
      }

      let cardCollection: CardCollectionData = FantastecSwapData.cardCollectionData[cardCollectionId]!
      if (cardCollection.isDeactivated){
        panic("cannot create cardData when cardCollectionId is inactive")
      }

      self.name = name
      self.level = level
      self.cardType = cardType
      self.aspectRatio = aspectRatio
      self.mediaData = mediaData
      self.cardCollectionId = cardCollectionId
      self.editionMintVolume = {}
      self.isDeactivated = false

      if (metadata == nil){
        self.metadata = {}
      } else {
        self.metadata = metadata
      }

      // set the id
      if (id == nil){
        self.id = FantastecSwapData.nextCardId
        FantastecSwapData.nextCardId = FantastecSwapData.nextCardId + (1 as UInt64)
      } else {
        self.id = id!
      }
    }
    pub fun save(){
      if (self.id == nil){
        self.id = FantastecSwapData.nextCardId
        FantastecSwapData.nextCardId =  FantastecSwapData.nextCardId + (1 as UInt64)
      }
      FantastecSwapData.cardData[self.id] = self
    }
    pub fun deactivate(){
      self.isDeactivated = true
    }
    pub fun addEditionMintVolume(edition: UInt64, mintVolume: UInt64) {
      if (self.editionMintVolume[edition] != nil){
        panic("That edition ready has a mintVolumne assigned")
      }
      self.editionMintVolume[edition] = mintVolume
    }
  }

  /** MediaType shape of a media item associated with a Card */
  access(all) struct MediaType {
    pub let type: String      // currently ["PLAYER","LINEUP"]
    pub let mediaURL: String  // eg "https://s3bucket.aws.com/path/to/image/jpg" or "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=="
    pub let hashAlgo: String  // eg SHA256
    pub let hash: String      // eg 40a2ef27ec35668ac2dbe5b182f6d5159ee94b3d7fa5f40fad18ccbe423f8afd or 27c960a2b437053a3714ec7ce4a074a96fa4d134f58c554cdd221a4722811fdc
    init(type: String, mediaURL: String, hashAlgo: String, hash: String){
      self.type = type
      self.mediaURL = mediaURL
      self.hashAlgo = hashAlgo
      self.hash = hash
    }
  }

  pub resource Admin {
    // Manage CardCollection functions
    pub fun addCardCollection(
      appId: String, 
      title: String, 
      shortTitle: String, 
      description: String, 
      level: String,
      metadata: {String: String},
      marketplaceFee: UFix64,
    ): CardCollectionData {

      var newCardCollection: CardCollectionData = CardCollectionData(
        appId: appId,
        title: title,
        shortTitle: shortTitle,
        description: description,
        level: level,
        metadata: metadata,
        marketplaceFee: marketplaceFee,
        id: nil,
      )

      newCardCollection.save()

      emit CardCollectionCreated(item: newCardCollection)

      return newCardCollection
    }
    pub fun updateCardCollectionById(
      appId: String, 
      title: String, 
      shortTitle: String, 
      description: String, 
      level: String,
      metadata: {String: String},
      marketplaceFee: UFix64,
      id: UInt64,
    ): CardCollectionData {

      // ensure the collection exists
      let cardCollection: CardCollectionData = FantastecSwapData.getCardCollectionById(id: id)
        ?? panic("No CardCollection found with id: ".concat(id.toString()))

      // ensure the collection is not deactivated
      if (cardCollection.isDeactivated == true){
        panic("CardCollection has been deactivated, updates are not allowed")
      }

      // create a new instance, which automatically updates the data dictionaries
      var newCardCollection: CardCollectionData = CardCollectionData(
        appId: appId,
        title: title,
        shortTitle: shortTitle,
        description: description,
        level: level,
        metadata: metadata,
        marketplaceFee: marketplaceFee,
        id: id,
      )

      newCardCollection.save()

      emit CardCollectionUpdated(item: newCardCollection)
      return newCardCollection
    }
    pub fun deactivateCardCollectionById(id: UInt64): Bool {
      // ensure the collection exists
      let cardCollection: CardCollectionData = FantastecSwapData.getCardCollectionById(id: id)
        ?? panic("No CardCollection found with id: ".concat(id.toString()))      
      cardCollection.deactivate()
      cardCollection.save()

      // deactivate all cards linked to this contract
      for cardId in FantastecSwapData.cardData.keys {
        let card: CardData = FantastecSwapData.cardData[cardId]!
        if (card.cardCollectionId == id) {
          self.deactivateCardById(id: card.id)
        }
      }

      emit CardCollectionDeactivated(id: id)
      return cardCollection.isDeactivated
    }
    pub fun removeAllCardCollections(): Bool {
      FantastecSwapData.cardCollectionData = {}
      return true
    }

    // Manage Card functions
    pub fun addCard(
      name: String, 
      level: String, 
      cardType: String, 
      aspectRatio: String, 
      metadata: {String: String},
      // mediaData: [MediaType],
      mediaData: String,
      cardCollectionId: UInt64
    ): CardData {

      var newCard: CardData = CardData(
        name: name,
        level: level,
        cardType: cardType,
        aspectRatio: aspectRatio,
        metadata: metadata,
        mediaData: mediaData,
        cardCollectionId: cardCollectionId,
        id: nil
      )

      newCard.save()

      emit CardCreated(item: newCard)

      return newCard
    }
    pub fun updateCardById(
      name: String, 
      level: String, 
      cardType: String, 
      aspectRatio: String, 
      metadata: {String: String},
      mediaData: String,
      cardCollectionId: UInt64,
      id: UInt64,
    ): CardData {
      pre {
        FantastecSwapData.getCardById(id: id) != nil: "Card not found with id: ".concat(id.toString())
        FantastecSwapData.cardCollectionData[cardCollectionId] != nil: "CardCollection not found with cardCollectionId: ".concat(cardCollectionId.toString())
      }
      let card: CardData = FantastecSwapData.getCardById(id: id)!
      if (card.isDeactivated){
        panic("Card has been deactivated, updates are not allowed")
      }

      var updatedCard: CardData = CardData(
        name: name,
        level: level,
        cardType: cardType,
        aspectRatio: aspectRatio,
        metadata: metadata,
        mediaData: mediaData,
        cardCollectionId: cardCollectionId,
        id: id
      )

      updatedCard.save()

      emit CardUpdated(item: updatedCard)

      return updatedCard
    }
    pub fun addEditionMintVolume(cardId: UInt64, edition: UInt64, mintVolume: UInt64): Bool{
      pre {
        FantastecSwapData.cardData[cardId] != nil: "No card found with id: ".concat(cardId.toString())
        mintVolume > 0: "Mint volume must be greater than 0"
        edition > 0: "Edition must be greater than 0"
      }
      let card: CardData = FantastecSwapData.cardData[cardId]!

      // update edition with maximum number allowed to be minted
      card.addEditionMintVolume(edition: edition, mintVolume: mintVolume)
      card.save()

      emit AddedEditionMintVolume(item: card)

      return true
    }
    pub fun deactivateCardById(id: UInt64): Bool {
      pre {
        FantastecSwapData.cardData[id] != nil: "No card found with id: ".concat(id.toString())
      }
      // ensure the collection exists
      let card: CardData = FantastecSwapData.getCardById(id: id)!
      card.deactivate()
      card.save()
      emit CardDeactivated(id: id)
      return card.isDeactivated
    }
  }

  /** PUBLIC GETTING FUNCTIONS */
  // CardCollection functions
  pub fun getAllCardCollections():[FantastecSwapData.CardCollectionData]{
    var cardCollections:[FantastecSwapData.CardCollectionData] = []
    for key in self.cardCollectionData.keys {
      let cardCollection: FantastecSwapData.CardCollectionData = FantastecSwapData.cardCollectionData[key]!
      if (cardCollection != nil){
        if (!cardCollection.isDeactivated){
          cardCollections.append(cardCollection)
        }
      }
    }
    return cardCollections;
  }
  pub fun getCardCollectionById(id: UInt64): FantastecSwapData.CardCollectionData? {
    return FantastecSwapData.cardCollectionData[id]
  }
  pub fun getCardCollectionIds(): [UInt64] {
    var keys:[UInt64] = []
    for key in self.cardCollectionData.keys {
      let collection: FantastecSwapData.CardCollectionData = FantastecSwapData.cardCollectionData[key]!
      if (collection != nil){
        if (!collection.isDeactivated){
          keys.append(collection.id)
        }
      }
    }
    return keys;
  }

  // Card functions
  pub fun getAllCards():[FantastecSwapData.CardData]{
    var cards:[FantastecSwapData.CardData] = []
    for key in self.cardData.keys {
      let card: FantastecSwapData.CardData = FantastecSwapData.cardData[key]!
      if (card != nil){
        if (!card.isDeactivated){
          cards.append(card)
        }
      }
    }
    return cards;
  }
  pub fun getCardById(id: UInt64): FantastecSwapData.CardData? {
    return FantastecSwapData.cardData[id]
  }
  pub fun getCardIds(): [UInt64] {
    return FantastecSwapData.cardData.keys
  }

  init() {

    self.cardCollectionData = {}
    self.nextCardCollectionId = 1

    self.cardData = {}
    self.nextCardId = 1

    // set storage paths
    self.AdminStoragePath = /storage/FantastecSwapAdmin
    self.account.save<@Admin>(<- create Admin(), to: self.AdminStoragePath)

    emit ContractInitialized()
  }
}