import FantastecSwapData from 0x01

transaction(id: UInt64) {

  var admin: &FantastecSwapData.Admin

  prepare(acct: AuthAccount) {
    self.admin = acct.borrow<&FantastecSwapData.Admin>(from: FantastecSwapData.AdminStoragePath)
      ?? panic("No Admin found on account")
  }

  execute {
    // update CardCollection by providing the Id and all fields as you want them set
    let result: Bool = self.admin.deactivateCardCollectionById(id: id)
    log("CardCollection deactivated, result: ".concat(result ? "True" : "False"))
    return
  }
}