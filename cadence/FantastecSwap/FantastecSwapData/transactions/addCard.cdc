import FantastecSwapData from 0x01

transaction (
  name: String,
  level: String,
  cardType: String,
  aspectRatio: String,
  metadata: {String: String},
  mediaData: String,
  cardCollectionId: UInt64
  ) {

  var admin: &FantastecSwapData.Admin

  prepare(acct: AuthAccount) {
    self.admin = acct.borrow<&FantastecSwapData.Admin>(from: FantastecSwapData.AdminStoragePath)
      ?? panic("No Admin found on account")
  }

  execute {
    let card: FantastecSwapData.CardData = self.admin.addCard(
      name: name,
      level: level,
      cardType: cardType,
      aspectRatio: aspectRatio,
      metadata: metadata,
      mediaData: mediaData,
      cardCollectionId: cardCollectionId
    )
    log("Card added, id: ".concat(card.id.toString()))
  }
}
