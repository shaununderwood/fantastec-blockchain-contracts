import FantastecSwapData from 0x01

transaction (
  appId: String,
  title: String,
  shortTitle: String,
  description: String,
  level: String,
  metadata: {String: String},
  marketplaceFee: UFix64,
  id: UInt64,
  )  {

  var admin: &FantastecSwapData.Admin

  prepare(acct: AuthAccount) {
    self.admin = acct.borrow<&FantastecSwapData.Admin>(from: FantastecSwapData.AdminStoragePath)
      ?? panic("No Admin found on account")
  }

  execute {
    // update CardCollection by providing the Id and all fields as you want them set
    self.admin.updateCardCollectionById(
      appId: appId,
      title: title,
      shortTitle: shortTitle,
      description: description,
      level: level,
      metadata: metadata,
      marketplaceFee: marketplaceFee,
      id: id,
    )
  }
}