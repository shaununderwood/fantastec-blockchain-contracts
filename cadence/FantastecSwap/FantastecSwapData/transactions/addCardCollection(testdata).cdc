import FantastecSwapData from 0x01

transaction {

  var admin: &FantastecSwapData.Admin

  prepare(acct: AuthAccount) {
    self.admin = acct.borrow<&FantastecSwapData.Admin>(from: FantastecSwapData.AdminStoragePath)
      ?? panic("No Admin found on account")
  }

  execute {
    var counter = 10; // starting at 10 so fantastecId is alwasy 32 chars in left

    while (counter < 20) {

      let sCounter = counter.toString()

      let cardCollection: FantastecSwapData.CardCollectionData = self.admin.addCardCollection(
        fantastecId: "123456789012345678901234567890".concat(sCounter),
        appId: "SWAP",
        title: "Test Collection ".concat(sCounter),
        shortTitle: "Collection ".concat(sCounter),
        description: "Basic test collection - updated",
        level: "AMBER",
        metadata: {}
      )

      log("CardCollection updated, id: ".concat(cardCollection.id.toString()).concat("fantastecId:"))

      counter = counter + 1
    }
  }
}