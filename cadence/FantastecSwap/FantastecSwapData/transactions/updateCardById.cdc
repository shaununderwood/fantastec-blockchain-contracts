import FantastecSwapData from 0x01

transaction (
  name: String,
  level: String,
  cardType: String,
  aspectRatio: String,
  metadata: {String: String},
  mediaData: String,
  cardCollectionId: UInt64,
  id: UInt64,
  ) {

  var admin: &FantastecSwapData.Admin

  prepare(acct: AuthAccount) {
    self.admin = acct.borrow<&FantastecSwapData.Admin>(from: FantastecSwapData.AdminStoragePath)
      ?? panic("No Admin found on account")
  }

  execute {
    // update CardCollection by providing the Id and all fields as you want them set
    self.admin.updateCardById(
      name: name,
      level: level,
      cardType: cardType,
      aspectRatio: aspectRatio,
      metadata: metadata,
      mediaData: mediaData,
      cardCollectionId: cardCollectionId,
      id: id,
    )
  }
}
