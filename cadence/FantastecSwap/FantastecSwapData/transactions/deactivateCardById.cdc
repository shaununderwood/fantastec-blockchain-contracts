import FantastecSwapData from 0x01

transaction(id: UInt64) {

  var admin: &FantastecSwapData.Admin

  prepare(acct: AuthAccount) {
    self.admin = acct.borrow<&FantastecSwapData.Admin>(from: FantastecSwapData.AdminStoragePath)
      ?? panic("No Admin found on account")
  }

  execute {
    self.admin.deactivateCardById(id: id)
  }
}