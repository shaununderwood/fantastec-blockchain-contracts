import FantastecSwapData from 0x01

transaction (
  cardId: UInt64,
  edition: UInt64,
  mintVolume: UInt64,
  ) {

  var admin: &FantastecSwapData.Admin

  prepare(acct: AuthAccount) {
    self.admin = acct.borrow<&FantastecSwapData.Admin>(from: FantastecSwapData.AdminStoragePath)
      ?? panic("No Admin found on account")
  }

  execute {
    self.admin.addEditionMintVolume(
      cardId: cardId,
      edition: edition,
      mintVolume: mintVolume,
    )
  }
}
