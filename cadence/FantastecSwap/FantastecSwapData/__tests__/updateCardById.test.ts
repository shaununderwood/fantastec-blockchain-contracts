import * as path from 'path';
import * as t from "@onflow/types";
import {
  init,
  sendTransaction,
  deployContractByName,
  getTransactionCode,
  // emulator,
  getContractAddress,
  getAccountAddress,
  executeScript,
  getScriptCode,
} from "flow-js-testing/dist";
import { TransactionEvent, TransactionResult, Card, CardCollection } from "../../shared/interfaces";
import { card1, card2, correctCardData, objectToKeyValueArray, correctCardCollectionData } from '../../shared/sharedAssets';

import emulator from "../../shared/emulatorWithNoLimit";
import { makeId } from '../../utils';

const basePath = path.resolve(__dirname, "../../FantastecSwapData");
const port = 8080;
const internalEmulator = true; // TODO make env

const POSITION_NAME = 0;
const POSITION_LEVEL = 1;
const POSITION_CARD_TYPE = 2;
const POSITION_ASPECT_RATIO = 3;
const POSITION_METADATA = 4;
const POSITION_MEDIADATA = 5;
const POSITION_COLLECTIONID = 6;
const POSITION_ID = 7;
const VALUE = 0;

beforeAll(() => {
  init(basePath, port);
});

describe("Test updateCard", () => {
  const name = "updateCardById";
  let Alice;
  let signers;
  let FantastecSwapData;
  let addressMap;
  let code;
  let testCardData;
  let cardId;

  beforeEach(async () => {
    if (internalEmulator) {
      await emulator.start(port, false);
      // load contract
      const name = "FantastecSwapData";
      const to = await getAccountAddress("Alice");
      await deployContractByName({ name, to });
    }

    // set main account
    Alice = await getAccountAddress("Alice");

    // Set transaction signers
    signers = [Alice];

    // Set code
    FantastecSwapData = await getContractAddress("FantastecSwapData");
    addressMap = { FantastecSwapData };
    code = await getTransactionCode({ name, addressMap });
    
    // add CardCollection for this card
    const name2 = 'addCardCollection';
    const code2 = await getTransactionCode({ name: name2, addressMap });
    const args = correctCardCollectionData();
    const result: TransactionResult = await sendTransaction({ code: code2, signers, args });
    const event: TransactionEvent = result.events[0];
    const data: CardCollection = event.data.item as CardCollection;

    // add intiial card
    const name3 = "addCard";
    const code3 = await getTransactionCode({ name: name3, addressMap });
    const args3 = correctCardData();
    const result3: TransactionResult = await sendTransaction({ code: code3, signers, args: args3 });
    const event3: TransactionEvent = result.events[0];
    const data3: Card = event.data.item as Card;

    if (result3?.status !== 4) {
      throw (`adding Card with id: ${data3.id} failed`);
    }
    cardId = data3.id;
  });

  // Stop emulator, so it could be restarted
  afterEach(async () => {
    if (internalEmulator) return emulator.stop();
  });

  describe("it should throw an error when", () => {
    // specific field tests are performed during addCard, as it uses the same constructor function

    it("has a card id that doesn't exist", async () => {
      // Define arguments
      const args = correctCardData();
      args.push([cardId + 1, t.UInt64]);

      // Generate addressMap from import statements
      let code = await getTransactionCode({ name, addressMap });

      const fragment = "FantastecSwapData.getCardById(id: id) != nil";
      await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
    });
    it("has a cardCollectionId that doesn't exist", async () => {
      // Define arguments
      const args = correctCardData();
      args.push([cardId, t.UInt64]);
      args[POSITION_COLLECTIONID][VALUE] += 1;

      // Generate addressMap from import statements
      let code = await getTransactionCode({ name, addressMap });

      const fragment = "FantastecSwapData.cardCollectionData[cardCollectionId] != nil";
      await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
    });
    it("tries to update a deactivated card", async () => {
      // deactivate card1
      const args2 = [[cardId, t.UInt64]];
      const name2 = "deactivateCardById";
      const code2 = await getTransactionCode({ name: name2, addressMap });
      await sendTransaction({ code: code2, signers, args: args2 });
      await expect(sendTransaction({ code: code2, signers, args: args2 })).resolves.toBeTruthy();
      
      // test can we update the card name
      // Define arguments
      const args = correctCardData();
      args[POSITION_NAME][VALUE] = "NFT Wonder Stuff!";
      args.push([cardId, t.UInt64]);

      // Generate addressMap from import statements
      let code = await getTransactionCode({ name, addressMap });

      const fragment = "Card has been deactivated, updates are not allowed";
      await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
    });
  });

  describe("when updating", () => {
    it("should allow updates to name, level, cardType, aspectRatio, metadata, mediaData", async () => {

      // Define arguments, use card2 updatable fields, and use existing card PK/FKs
      const args = correctCardData(card2);
      args[POSITION_COLLECTIONID][VALUE] = card1.cardCollectionId;
      args.push([cardId, t.UInt64]);

      // Generate addressMap from import statements
      let code = await getTransactionCode({ name, addressMap });

      // execute update
      await expect(sendTransaction({ code, signers, args })).resolves.toBeTruthy();

      // get result
      const name2 = "getCardById";
      const code2 = await getScriptCode({ name: name2, addressMap });
      const args2 = [[cardId, t.UInt64]];
      const data = await executeScript({ code: code2, args: args2 });
      
      // test result
      expect(data.name).toBe(card2.name);
      expect(data.level).toBe(card2.level);
      expect(data.cardType).toBe(card2.cardType);
      expect(data.aspectRatio).toBe(card2.aspectRatio);
      expect(data.metadata).toEqual(card2.metadata);
      expect(data.mediaData).toEqual(card2.mediaData);
      expect(data.mediaData).toEqual(card2.mediaData);
      expect(data.cardCollectionId).toEqual(args[POSITION_COLLECTIONID][VALUE]);
      expect(data.id).toEqual(cardId);      
    });
  });
});
