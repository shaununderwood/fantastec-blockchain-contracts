// addEditionMintVolume
import * as path from 'path';
import * as t from "@onflow/types";
import {
  init,
  sendTransaction,
  deployContractByName,
  getTransactionCode,
  // emulator,
  getContractAddress,
  getAccountAddress,
  getScriptCode,
  executeScript,
} from "flow-js-testing/dist";
import { TransactionEvent, TransactionResult, CardCollection, Card } from "../../shared/interfaces";
import { correctCardCollectionData, correctCardData, card1 as exampleCard1, card2 as exampleCard2 } from '../../shared/sharedAssets';

import emulator from "../../shared/emulatorWithNoLimit";

const basePath = path.resolve(__dirname, "../../FantastecSwapData");
const port = 8080;
const internalEmulator = true; // TODO make env

beforeAll(() => {
  init(basePath, port);
});

describe("Test addEditionMintVolume", () => {
  let Alice;
  let signers;
  let FantastecSwapData;
  let addressMap;
  let cardId1;
  let cardId2;

  beforeEach(async () => {
    if (internalEmulator) {
      await emulator.start(port, false);
      // load contract
      const name = "FantastecSwapData";
      const to = await getAccountAddress("Alice");
      await deployContractByName({ name, to });
    }

    // set main account
    Alice = await getAccountAddress("Alice");

    // Set transaction signers
    signers = [Alice];

    FantastecSwapData = await getContractAddress("FantastecSwapData");
    addressMap = { FantastecSwapData };

    // add CardCollection for this card
    const name = "addCardCollection";
    const code = await getTransactionCode({ name, addressMap });
    const args = correctCardCollectionData();
    await expect(sendTransaction({ code, signers, args })).resolves.toBeTruthy();

    // Add a Card to update
    const args1 = correctCardData(exampleCard1);
    const name1 = "addCard";
    const code1 = await getTransactionCode({ name: name1, addressMap });
    const result1: TransactionResult = await sendTransaction({ code: code1, signers, args: args1 });
    const event1: TransactionEvent = result1.events[0];
    const card1: Card = event1.data.item as Card;
    expect(result1.status).toBe(4);
    cardId1 = card1.id;

    // Add a Card to update
    const args2 = correctCardData(exampleCard2);
    const name2 = "addCard";
    const code2 = await getTransactionCode({ name: name2, addressMap });
    const result2: TransactionResult = await sendTransaction({ code: code2, signers, args: args2 });
    const event2: TransactionEvent = result2.events[0];
    const card2: Card = event2.data.item as Card;
    expect(result2.status).toBe(4);
    cardId2 = card2.id;
  });

  // Stop emulator, so it could be restarted
  afterEach(async () => {
    if (internalEmulator) return emulator.stop();
  });

  describe("allows the addition of an edition and mint volume to a card", () => {
    const name = "addEditionMintVolume";
    let code;

    beforeEach(async () => {
      code = await getTransactionCode({ name, addressMap });
    });
    function makeArgs(cardId, edition, volume) {
      return [
        [cardId, t.UInt64],
        [edition, t.UInt64],
        [volume, t.UInt64],
      ];
    }

    it("saves one edition:mintVolume", async () => {
      let edition = 1;
      let mintVolume = 10000;
      const args = makeArgs(cardId1, edition, mintVolume);
      const editionMintVolume = { [`${edition}`]: mintVolume };
      const results = await sendTransaction({ code, signers, args });
      expect(results.events[0].data.item.editionMintVolume).toEqual(editionMintVolume);
    });

    it("saves another edition:mintVolume", async () => {
      let edition = 2;
      let mintVolume = 50000;
      const args = makeArgs(cardId1, edition, mintVolume);
      const editionMintVolume = { [`${edition}`]: mintVolume };
      const results = await sendTransaction({ code, signers, args });
      expect(results.events[0].data.item.editionMintVolume).toEqual(editionMintVolume);
    });

    it("saves edition:mintVolume for card2", async () => {
      let edition = 1;
      let mintVolume = 100000;
      const args = makeArgs(cardId2, edition, mintVolume);
      const editionMintVolume = { [`${edition}`]: mintVolume };
      const results = await sendTransaction({ code, signers, args });
      expect(results.events[0].data.item.editionMintVolume).toEqual(editionMintVolume);
    });

    it("has saved all entries, which are retrievable with getCardById", async () => {
      // add all the editions and mint volumes
      let edition = 1;
      let mintVolume = 10000;
      let args = makeArgs(cardId1, edition, mintVolume);
      await expect(sendTransaction({ code, signers, args })).resolves.toBeTruthy();
      edition = 2;
      mintVolume = 50000;
      args = makeArgs(cardId1, edition, mintVolume);
      await expect(sendTransaction({ code, signers, args })).resolves.toBeTruthy();
      edition = 1;
      mintVolume = 100000;
      args = makeArgs(cardId2, edition, mintVolume);
      await expect(sendTransaction({ code, signers, args })).resolves.toBeTruthy();

      // get all cards and check their edition:mintVolume figures
      const name = "getAllCards";
      const code2 = await getScriptCode({ name, addressMap });
      const allCards = await executeScript({ code: code2 });
      expect(allCards[0].editionMintVolume).toEqual({ "1": 10000, "2": 50000 });
      expect(allCards[1].editionMintVolume).toEqual({ "1": 100000 });
    });
  });

  describe("throws an error when", () => {
    it.todo("the requested edition is more than 1 edition above the current highest");
    it.todo("receives a mint volume equal to 0");
    it.todo("receives a edition equal to 0");
    it.todo("card is deactivated");
  });

  // describe("after deactivating a Card", () => {
  //   beforeEach(async () => {
  //     // deactivate Card
  //     const name = "deactivateCardById";
  //     const code = await getTransactionCode({ name, addressMap });
  //     const args = [[cardId, t.UInt64]];
  //     await expect(sendTransaction({ code, signers, args })).resolves.toHaveProperty("status", 4);
  //   });

  //   it("should be available by direct requesting using it's id", async () => {
  //     // get Card and check it's deactivated
  //     const name = "getCardById";
  //     const code = await getScriptCode({ name, addressMap });
  //     const args = [[cardId, t.UInt64]];
  //     await expect(executeScript({ code, args })).resolves.toHaveProperty("isDeactivated", true);
  //   });

  //   it("should not be available when getting all cards", async () => {
  //     const name = "getAllCards";
  //     const code = await getScriptCode({ name, addressMap });
  //     const args = [];
  //     await expect(executeScript({ code, args })).resolves.toHaveLength(0);
  //   });
  // });
});
