import * as path from 'path';
import * as t from "@onflow/types";
import {
  init,
  sendTransaction,
  deployContractByName,
  getTransactionCode,
  // emulator,
  getContractAddress,
  getAccountAddress,
  getScriptCode,
  executeScript,
} from "flow-js-testing/dist";
import { TransactionEvent, TransactionResult, CardCollection } from "../../shared/interfaces";
import { cardCollection1, cardCollection2, correctCardCollectionData } from '../../shared/sharedAssets';

import emulator from "../../shared/emulatorWithNoLimit";

const basePath = path.resolve(__dirname, "../../FantastecSwapData");
const port = 8080;
const internalEmulator = true; // TODO make env
const c1 = cardCollection1;
const c2 = cardCollection2;

beforeAll(() => {
  init(basePath, port);
});

describe("Test getCardCollectionIds", () => {
  const nameAddCardCollection = "addCardCollection";
  let Alice;
  let signers;
  let FantastecSwapData;
  let addressMap;
  let cardCollectionId1;
  let cardCollectionId2;

  beforeEach(async () => {
    if (internalEmulator) {
      await emulator.start(port, false);
      // load contract
      const name = "FantastecSwapData";
      const to = await getAccountAddress("Alice");
      await deployContractByName({ name, to });
    }

    // set main account
    Alice = await getAccountAddress("Alice");

    // Set transaction signers
    signers = [Alice];

    FantastecSwapData = await getContractAddress("FantastecSwapData");
    addressMap = { FantastecSwapData };

    // Add CardCollection 1 to be retrieved
    const args1 = correctCardCollectionData(c1);
    const code1 = await getTransactionCode({ name: nameAddCardCollection, addressMap });
    const resultAddCollection1: TransactionResult = await sendTransaction({ code: code1, signers, args: args1 });
    const eventAddCollection1: TransactionEvent = resultAddCollection1.events[0];
    const id1 = (eventAddCollection1.data.item as CardCollection).id;
    expect(resultAddCollection1?.status).toBe(4);
    cardCollectionId1 = id1;

    // Add CardCollection 1 to be retrieved
    const args2 = correctCardCollectionData(c2);
    const code2 = await getTransactionCode({ name: nameAddCardCollection, addressMap });
    const resultAddCollection2: TransactionResult = await sendTransaction({ code: code2, signers, args: args2 });
    const eventAddCollection2: TransactionEvent = resultAddCollection2.events[0];
    const id2 = (eventAddCollection2.data.item as CardCollection).id;
    expect(resultAddCollection2?.status).toBe(4);
    cardCollectionId2 = id2;
  });

  // Stop emulator, so it could be restarted
  afterEach(async () => {
    if (internalEmulator) return emulator.stop();
  });

  test("script returns two entries", async () => {
    const name = "getCardCollectionIds";
    let code = await getScriptCode({ name, addressMap });
    const args = [];
    await expect(executeScript({ code, args })).resolves.toHaveLength(2);
  });

  describe("after deactivating one entry", () => {
    const name = "getCardCollectionIds";
    let code;
    const args = [];

    beforeEach(async () => {
      code = await getScriptCode({ name, addressMap });
    });

    beforeEach(async () => {
      // deactivate one of the test assets
      const name = "deactivateCardCollectionById";
      const code = await getTransactionCode({ name, addressMap });
      const args = [[cardCollectionId1, t.UInt64]];
      await expect(sendTransaction({ code, signers, args })).resolves.toHaveProperty("status", 4);
    });

    it("returns an array", async () => {
      await expect(executeScript({ code, args })).resolves.toBeInstanceOf(Array);
    });

    it("contains the collection's id", async () => {
      await expect(executeScript({ code, args })).resolves.toContain(cardCollectionId2);
    });
  });
});
