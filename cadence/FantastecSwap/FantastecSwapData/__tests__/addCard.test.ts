import * as path from 'path';
import * as t from "@onflow/types";
import {
  init,
  sendTransaction,
  deployContractByName,
  getTransactionCode,
  // emulator,
  getContractAddress,
  getAccountAddress,
  getScriptCode,
  executeScript,
} from "flow-js-testing/dist";
import { TransactionEvent, TransactionResult, Card, CardCollection } from "../../shared/interfaces";
import { card1, card2, correctCardData, objectToKeyValueArray, correctCardCollectionData } from '../../shared/sharedAssets';

import emulator from "../../shared/emulatorWithNoLimit";

const basePath = path.resolve(__dirname, "../../FantastecSwapData");
const port = 8080;
const internalEmulator = true; // TODO make env

const POSITION_NAME = 0;
const POSITION_LEVEL = 1;
const POSITION_CARD_TYPE = 2;
const POSITION_ASPECT_RATIO = 3;
const POSITION_METADATA = 4;
const POSITION_MEDIADATA = 5;
const POSITION_COLLECTIONID = 6;
const VALUE = 0;

beforeAll(() => {
  init(basePath, port);
});

describe("Test addCard", () => {
  const name = "addCard";
  let Alice;
  let signers;
  let FantastecSwapData;
  let addressMap;
  let code;
  let testCardData;

  beforeEach(async () => {
    if (internalEmulator) {
      await emulator.start(port, false);
      // load contract
      const name = "FantastecSwapData";
      const to = await getAccountAddress("Alice");
      await deployContractByName({ name, to });
    }

    // set main account
    Alice = await getAccountAddress("Alice");

    // Set transaction signers
    signers = [Alice];

    // Set code
    FantastecSwapData = await getContractAddress("FantastecSwapData");
    addressMap = { FantastecSwapData };
    code = await getTransactionCode({ name, addressMap });

    // add CardCollection for this card
    const name2 = 'addCardCollection';
    const code2 = await getTransactionCode({ name: name2, addressMap });
    const args = correctCardCollectionData();
    const result: TransactionResult = await sendTransaction({ code: code2, signers, args });
    const event: TransactionEvent = result.events[0];
    const data: CardCollection = event.data.item as CardCollection;

    // update card with cardCollection.id
    testCardData = correctCardData();
    testCardData[POSITION_COLLECTIONID][VALUE] = data.id;
  });

  // Stop emulator, so it could be restarted
  afterEach(async () => {
    if (internalEmulator) return emulator.stop();
  });

  test("a new card will be added to the contract", async () => {
    // Define arguments
    const args = testCardData;

    try {
      const result: TransactionResult = await sendTransaction({ code, signers, args });
      const event: TransactionEvent = result.events[0];
      const data: Card = event.data.item as Card;

      // could be a more fuzzy equality, as isDeactived is a property on Card on chain
      const c1 = card1;
      expect(data.id).toBe(1);
      expect(data.name).toEqual(c1.name);
      expect(data.aspectRatio).toBe(c1.aspectRatio);
      expect(data.cardCollectionId).toBe(c1.cardCollectionId);
      expect(data.cardType).toBe(c1.cardType);
      expect(data.level).toBe(c1.level);
      expect(data.metadata).toEqual(c1.metadata);
      expect(data.mediaData).toBe(c1.mediaData);

    } catch (e) {
      console.log('ERROR=', e);
      expect(e).toBeUndefined(); // seems a logical way to elicit an error
    }
  });

  describe("it should error when field `name` ", () => {
    test("is not long enough", async () => {
      // Define arguments
      const args = correctCardData();
      args[POSITION_NAME][VALUE] = "";

      // Generate addressMap from import statements
      let code = await getTransactionCode({ name, addressMap });

      const fragment = "name.length > 0";
      await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
    });
  });

  describe("it should error when field `level` ", () => {
    test("is not long enough", async () => {
      // Define arguments
      const args = correctCardData();
      args[POSITION_LEVEL][VALUE] = "";

      // Generate addressMap from import statements
      let code = await getTransactionCode({ name, addressMap });

      const fragment = "level.length > 0";
      await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
    });
  });

  describe("it should error when field `cardType` ", () => {
    test("is not long enough", async () => {
      // Define arguments
      const args = correctCardData();
      args[POSITION_CARD_TYPE][VALUE] = "";

      // Generate addressMap from import statements
      let code = await getTransactionCode({ name, addressMap });

      const fragment = "cardType.length > 0";
      await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
    });
  });

  describe("it should error when field `aspectRatio` ", () => {
    test("is not long enough", async () => {
      // Define arguments
      const args = correctCardData();
      args[POSITION_ASPECT_RATIO][VALUE] = "";

      // Generate addressMap from import statements
      let code = await getTransactionCode({ name, addressMap });

      const fragment = "aspectRatio.length > 0";
      await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
    });
  });

  describe("it should error when field `mediaData` ", () => {
    test("is not long enough", async () => {
      // Define arguments
      const args = correctCardData();
      const NO_MEDIA = '';
      args[POSITION_MEDIADATA][VALUE] = NO_MEDIA;

      // Generate addressMap from import statements
      let code = await getTransactionCode({ name, addressMap });

      const fragment = "mediaData.length > 0";
      await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
    });
  });

  describe("it should error when field `cardCollection` ", () => {
    test("is less than 1", async () => {
      // Define arguments
      const args = correctCardData();
      args[POSITION_COLLECTIONID][VALUE] = 0;

      // Generate addressMap from import statements
      let code = await getTransactionCode({ name, addressMap });

      const fragment = "cardCollectionId > (0 as UInt64)";
      await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
    });

    test("doesn't relate to an existing CardCollection", async () => {
      // Define arguments
      const args = correctCardData();
      args[POSITION_COLLECTIONID][VALUE] = 99;

      // Generate addressMap from import statements
      let code = await getTransactionCode({ name, addressMap });

      const fragment = "cannot create cardData when cardCollectionId does not exist";
      await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
    });
  });

  describe("when card is added to a cardCollection which is deactivated", () => {
    let cardCollectionId;

    beforeEach(async () => {
      // Add a CardCollection to be deactivated
      const name1 = "addCardCollection";
      const args1 = correctCardCollectionData();
      const code1 = await getTransactionCode({ name: name1, addressMap });
      const resultAddCollection: TransactionResult = await sendTransaction({ code: code1, signers, args: args1 });
      const eventAddCollection: TransactionEvent = resultAddCollection.events[0];
      const { id }: CardCollection = eventAddCollection.data.item as CardCollection;
      cardCollectionId = id;
      expect(cardCollectionId).toBeGreaterThan(0);

      // deactivate CardCollection
      const name2 = "deactivateCardCollectionById";
      const code2 = await getTransactionCode({ name: name2, addressMap });
      const args2 = [[cardCollectionId, t.UInt64]];
      await expect(sendTransaction({ code: code2, signers, args: args2 })).resolves.toHaveProperty("status", 4);
    });

    it("should throw an error", async () => {
      // Define arguments
      const args = correctCardData();
      args[POSITION_COLLECTIONID][VALUE] = cardCollectionId;

      // Generate addressMap from import statements
      let code = await getTransactionCode({ name, addressMap });

      const fragment = "cannot create cardData when cardCollectionId is inactive";
      await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
    });
  });

  describe("when a collection has cards and is deactivated", () => {
    let cardCollectionId;

    beforeEach(async () => {
      // Add a CardCollection to be deactivated
      const name1 = "addCardCollection";
      const args1 = correctCardCollectionData();
      const code1 = await getTransactionCode({ name: name1, addressMap });
      const resultAddCollection: TransactionResult = await sendTransaction({ code: code1, signers, args: args1 });
      const eventAddCollection: TransactionEvent = resultAddCollection.events[0];
      const { id }: CardCollection = eventAddCollection.data.item as CardCollection;
      cardCollectionId = id;
      expect(cardCollectionId).toBeGreaterThan(0);

      // Add card 1
      const name2 = "addCard"
      const args2 = correctCardData(card1);
      args2[POSITION_COLLECTIONID][VALUE] = cardCollectionId;
      let code2 = await getTransactionCode({ name: name2, addressMap });
      await expect(sendTransaction({ code: code2, signers, args: args2 })).resolves.toBeTruthy();

      // Add card 2
      const args3 = correctCardData(card2);
      args3[POSITION_COLLECTIONID][VALUE] = cardCollectionId;
      let code3 = await getTransactionCode({ name: name2, addressMap });
      await expect(sendTransaction({ code: code3, signers, args: args3 })).resolves.toBeTruthy();
    });

    it("should deactivate all associated cards", async () => {
      // get all cards
      const name1 = "getAllCards";
      const code1 = await getScriptCode({ name: name1, addressMap });
      await expect(executeScript({ code: code1 })).resolves.toHaveLength(2);

      // deactivate CardCollection
      const name2 = "deactivateCardCollectionById";
      const code2 = await getTransactionCode({ name: name2, addressMap });
      const args2 = [[cardCollectionId, t.UInt64]];
      await expect(sendTransaction({ code: code2, signers, args: args2 })).resolves.toHaveProperty("status", 4);

      // get all cards
      await expect(executeScript({ code: code1 })).resolves.toHaveLength(0);
    });

  });

});
