import * as path from 'path';
import * as t from "@onflow/types";
import {
  init,
  sendTransaction,
  deployContractByName,
  getTransactionCode,
  // emulator,
  getContractAddress,
  getAccountAddress,
  getScriptCode,
  executeScript,
} from "flow-js-testing/dist";
import { TransactionEvent, TransactionResult, CardCollection, Card } from "../../shared/interfaces";
import { correctCardCollectionData, correctCardData } from '../../shared/sharedAssets';

import emulator from "../../shared/emulatorWithNoLimit";

const basePath = path.resolve(__dirname, "../../FantastecSwapData");
const port = 8080;
const internalEmulator = true; // TODO make env

beforeAll(() => {
  init(basePath, port);
});

describe("Test getCardById", () => {
  let Alice;
  let signers;
  let FantastecSwapData;
  let addressMap;
  let cardId;

  beforeEach(async () => {
    if (internalEmulator) {
      await emulator.start(port, false);
      // load contract
      const name = "FantastecSwapData";
      const to = await getAccountAddress("Alice");
      await deployContractByName({ name, to });
    }

    // set main account
    Alice = await getAccountAddress("Alice");

    // Set transaction signers
    signers = [Alice];

    FantastecSwapData = await getContractAddress("FantastecSwapData");
    addressMap = { FantastecSwapData };

    // add CardCollection for this card
    const name1 = "addCardCollection";
    const code1 = await getTransactionCode({ name: name1, addressMap });
    const args1 = correctCardCollectionData();
    const result1: TransactionResult = await sendTransaction({ code: code1, signers, args: args1 });
    const event1: TransactionEvent = result1.events[0];
    const data1: CardCollection = event1.data.item as CardCollection;
  });

  // Stop emulator, so it could be restarted
  afterEach(async () => {
    if (internalEmulator) return emulator.stop();
  });

  describe("when getting a card by id, ", () => {
    let cards;
    let cardId;
    beforeEach(async () => {
      // Add a Card to be tested
      const args = correctCardData();
      const name = "addCard";
      const code = await getTransactionCode({ name, addressMap });
      const resultAddCard: TransactionResult = await sendTransaction({ code, signers, args });
      const eventAddCard: TransactionEvent = resultAddCard.events[0];
      const { id }: Card = eventAddCard.data.item as Card;
      expect(resultAddCard?.status).toBe(4);
      cardId = id;
    });

    it("should return a card with the same id", async () => {
      const name = "getCardById";
      const code = await getScriptCode({ name, addressMap });
      const args = [[cardId, t.UInt64]];
      await expect(executeScript({ code, args })).resolves.toHaveProperty("id", cardId);
    });

    it("should return nil if the card id isn't available", async () => {
      const name = "getCardById";
      const code = await getScriptCode({ name, addressMap });
      const args = [[cardId + 1, t.UInt64]];
      const result = await executeScript({ code, args })
      await expect(executeScript({ code, args })).resolves.toBe(null);
    });
  });
});
