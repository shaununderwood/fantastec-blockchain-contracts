import * as path from 'path';
import * as t from "@onflow/types";
import {
  init,
  sendTransaction,
  deployContractByName,
  getTransactionCode,
  // emulator,
  getContractAddress,
  getAccountAddress,
  getScriptCode,
  executeScript,
} from "flow-js-testing/dist";
import { TransactionEvent, TransactionResult, CardCollection } from "../../shared/interfaces";
import { cardCollection1, cardCollection2, correctCardCollectionData, UFIX64_DECIMAL_PLACES } from '../../shared/sharedAssets';

import emulator from "../../shared/emulatorWithNoLimit";

const basePath = path.resolve(__dirname, "../../FantastecSwapData");
const port = 8080;
const internalEmulator = true; // TODO make env
const c1 = cardCollection1;
const c2 = cardCollection2;

beforeAll(() => {
  init(basePath, port);
});

describe("Test updateCardCollectionById", () => {
  let Alice;
  let signers;
  let FantastecSwapData;
  let addressMap;
  let cardCollectionId;

  beforeEach(async () => {
    if (internalEmulator) {
      await emulator.start(port, false);
      // load contract
      const name = "FantastecSwapData";
      const to = await getAccountAddress("Alice");
      await deployContractByName({ name, to });
    }

    // set main account
    Alice = await getAccountAddress("Alice");

    // Set transaction signers
    signers = [Alice];

    FantastecSwapData = await getContractAddress("FantastecSwapData");
    addressMap = { FantastecSwapData };

    // Add CardCollection to be retrieved
    const args = correctCardCollectionData(c1);
    const name = "addCardCollection";
    const code = await getTransactionCode({ name, addressMap });
    const resultAddCollection: TransactionResult = await sendTransaction({ code, signers, args });
    const eventAddCollection: TransactionEvent = resultAddCollection.events[0];
    const id = (eventAddCollection.data.item as CardCollection).id;

    // might be able to add expect() in this block
    if (resultAddCollection?.status !== 4) {
      throw (`adding CardCollection with id: ${id} failed`);
    }
    cardCollectionId = id;
  });

  // Stop emulator, so it could be restarted
  afterEach(async () => {
    if (internalEmulator) return emulator.stop();
  });

  test("to ensure an unknown CardCollection id causes an error", async () => {
    const args = correctCardCollectionData(cardCollection2);
    // ensure id is invalid by incrementing added collection.id by 1
    args.push([cardCollectionId + 1, t.UInt64]);
    const name = "updateCardCollectionById";
    const code = await getTransactionCode({ name, addressMap });
    await expect(sendTransaction({ code, signers, args })).rejects.toMatch(/No CardCollection found with id/);
  });

  describe("to ensure deactivated CardCollection ids", () => {
    beforeEach(async () => {
      let name = "deactivateCardCollectionById";
      let args = [[cardCollectionId, t.UInt64]];
      let code = await getTransactionCode({ name, addressMap });
      await expect(sendTransaction({ code, signers, args })).resolves.toHaveProperty("status", 4)
    })

    it("cannot be updated", async () => {
      // update c1 with c2's properties, with id collected previously
      const args = correctCardCollectionData(cardCollection2);

      // ensure id is invalid by incrementing added collection.id by 1
      args.push([cardCollectionId + 1, t.UInt64]);
      const name = "updateCardCollectionById";
      const code = await getTransactionCode({ name, addressMap });
      await expect(sendTransaction({ code, signers, args })).rejects.toMatch(/No CardCollection found with id/);
    });
  });

  describe("to ensure all CardCollection fields", () => {
    beforeEach(async () => {
      // update c1 with c2's properties, with id collected previously
      let args = correctCardCollectionData(c2);
      args.push([cardCollectionId, t.UInt64]);
      let name = "updateCardCollectionById";
      const code = await getTransactionCode({ name, addressMap });
      await expect(sendTransaction({ code, signers, args })).resolves.toHaveProperty("status", 4);
    });

    it("are updated", async () => {
      const name = "getCardCollectionById";
      const code = await getScriptCode({ name, addressMap });
      const args = [[cardCollectionId, t.UInt64]];
      const updated: CardCollection = await executeScript({ code, args });
      expect(updated.id).toBe(cardCollectionId);
      expect(updated.appId).toBe(c2.appId);
      expect(updated.description).toBe(c2.description);
      expect(updated.isDeactivated).toBe(false);
      expect(updated.level).toBe(c2.level);
      expect(updated.shortTitle).toBe(c2.shortTitle);
      expect(updated.title).toBe(c2.title);
      expect(updated.metadata).toEqual(c2.metadata);
      expect(updated.marketplaceFee).toEqual(c2.marketplaceFee.toFixed(UFIX64_DECIMAL_PLACES));
    });
  });
});
