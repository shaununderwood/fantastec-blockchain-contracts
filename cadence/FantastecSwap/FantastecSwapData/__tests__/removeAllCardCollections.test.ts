import * as path from 'path';
import * as t from "@onflow/types";
import {
  init,
  sendTransaction,
  deployContractByName,
  getTransactionCode,
  // emulator,
  getContractAddress,
  getAccountAddress,
  getScriptCode,
  executeScript
} from "flow-js-testing/dist";
import { TransactionEvent, TransactionResult, CardCollection } from "../../shared/interfaces";
import { cardCollection1, correctCardCollectionData } from '../../shared/sharedAssets';

import emulator from "../../shared/emulatorWithNoLimit";

const basePath = path.resolve(__dirname, "../../FantastecSwapData");
const port = 8080;
const internalEmulator = true; // TODO make env
const TRANSACTION_STATUS = {
  SUCCESS: 4
}

beforeAll(() => {
  init(basePath, port);
});

describe("Test removeAllCardCollections", () => {
  let Alice;
  let signers;
  let FantastecSwapData;
  let addressMap;

  beforeEach(async () => {
    if (internalEmulator) {
      await emulator.start(port, false);
      // load contract
      const name = "FantastecSwapData";
      const to = await getAccountAddress("Alice");
      await deployContractByName({ name, to });
    }

    // set main account
    Alice = await getAccountAddress("Alice");

    // Set transaction signers
    signers = [Alice];

    FantastecSwapData = await getContractAddress("FantastecSwapData");
    addressMap = { FantastecSwapData };

    // Add CardCollection to be retrieved
    const args = correctCardCollectionData(cardCollection1);
    const name = "addCardCollection";
    const code = await getTransactionCode({ name, addressMap });
    await expect(sendTransaction({ code, signers, args })).resolves.toHaveProperty("status", TRANSACTION_STATUS.SUCCESS);
  });

  // Stop emulator, so it could be restarted
  afterEach(async () => {
    if (internalEmulator) return emulator.stop();
  });

  describe("when cardCollections is not empty", () => {

    beforeEach(async () => {
      const name = "getCardCollectionIds";
      const code = await getScriptCode({ name, addressMap });
      const args = [];
      await expect(executeScript({ code, args })).resolves.toHaveLength(1);
    })

    it("executes removeAllCardCollections, leaving it empty", async () => {
      const name = "removeAllCardCollections";
      const code = await getTransactionCode({ name, addressMap });
      const args = [];
      await expect(sendTransaction({ code, signers, args })).resolves.toHaveProperty("status", 4);

      const name2 = "getCardCollectionIds";
      let code2 = await getScriptCode({ name: name2, addressMap });
      const args2 = [];
      await expect(executeScript({ code: code2, args: args2 })).resolves.toHaveLength(0);
    });
  });
});
