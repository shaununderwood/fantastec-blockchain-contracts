import * as path from 'path';
import * as t from "@onflow/types";
import {
  init,
  sendTransaction,
  deployContractByName,
  getTransactionCode,
  // emulator,
  getContractAddress,
  getAccountAddress,
  getScriptCode,
  executeScript,
} from "flow-js-testing/dist";
import { TransactionEvent, TransactionResult, CardCollection } from "../../shared/interfaces";
import { cardCollection1, cardCollection2, correctCardCollectionData } from '../../shared/sharedAssets';

import emulator from "../../shared/emulatorWithNoLimit";

const basePath = path.resolve(__dirname, "../../FantastecSwapData");
const port = 8080;
const internalEmulator = true; // TODO make env

beforeAll(() => {
  init(basePath, port);
});

describe("Test getCardCollectionById", () => {
  const nameAddCardCollection = "addCardCollection";
  let Alice;
  let signers;
  let FantastecSwapData;
  let addressMap;
  let cardCollectionId1;
  let cardCollectionId2;

  beforeEach(async () => {
    if (internalEmulator) {
      await emulator.start(port, false);
      // load contract
      const name = "FantastecSwapData";
      const to = await getAccountAddress("Alice");
      await deployContractByName({ name, to });
    }

    // set main account
    Alice = await getAccountAddress("Alice");

    // Set transaction signers
    signers = [Alice];

    FantastecSwapData = await getContractAddress("FantastecSwapData");
    addressMap = { FantastecSwapData };

    // Add CardCollection 1 to be retrieved
    const c1 = cardCollection1;
    const args1 = correctCardCollectionData(c1);
    const code1 = await getTransactionCode({ name: nameAddCardCollection, addressMap });
    const resultAddCollection1: TransactionResult = await sendTransaction({ code: code1, signers, args: args1 });
    const eventAddCollection1: TransactionEvent = resultAddCollection1.events[0];
    const id1 = (eventAddCollection1.data.item as CardCollection).id;
    if (resultAddCollection1?.status !== 4) {
      throw (`adding CardCollection with id: ${id1} failed`);
    }
    cardCollectionId1 = id1;

    // Add CardCollection 1 to be retrieved
    const c2 = cardCollection2;
    const args2 = correctCardCollectionData(c2);
    const code2 = await getTransactionCode({ name: nameAddCardCollection, addressMap });
    const resultAddCollection2: TransactionResult = await sendTransaction({ code: code2, signers, args: args2 });
    const eventAddCollection2: TransactionEvent = resultAddCollection2.events[0];
    const id2 = (eventAddCollection2.data.item as CardCollection).id;
    if (resultAddCollection2?.status !== 4) {
      throw (`adding CardCollection with id: ${id2} failed`);
    }
    cardCollectionId2 = id2;
  });

  // Stop emulator, so it could be restarted
  afterEach(async () => {
    if (internalEmulator) return emulator.stop();
  });

  it("should return null when id not present", async () => {
    const name = "getCardCollectionById";

    let code = await getScriptCode({ name, addressMap });

    // Define arguments
    const cardCollectibleId = 100;
    const args = [[cardCollectibleId, t.UInt64]];

    await expect(executeScript({ code, args })).resolves.toBeNull();
  });

  it("should pull back the specific record", async () => {
    const name = "getCardCollectionById";
    let code = await getScriptCode({ name, addressMap });

    // Define arguments
    const args1 = [[cardCollectionId1, t.UInt64]];
    const args2 = [[cardCollectionId2, t.UInt64]];

    await expect(executeScript({ code, args: args1 })).resolves.toHaveProperty("id", cardCollectionId1);
    await expect(executeScript({ code, args: args2 })).resolves.toHaveProperty("id", cardCollectionId2);
  });
});
