import * as path from 'path';
import * as t from "@onflow/types";
import {
  init,
  sendTransaction,
  deployContractByName,
  getTransactionCode,
  // emulator,
  getContractAddress,
  getAccountAddress
} from "flow-js-testing/dist";
import { TransactionEvent, TransactionResult, CardCollection } from "../../shared/interfaces";
import { cardCollection1, correctCardCollectionData, UFIX64_DECIMAL_PLACES } from '../../shared/sharedAssets';

import emulator from "../../shared/emulatorWithNoLimit";

const basePath = path.resolve(__dirname, "../../FantastecSwapData");
const port = 8080;
const internalEmulator = true; // TODO make env

const VALUE = 0;
const POSITION_APP_ID = 0;
const POSITION_TITLE = 1;
const POSITION_SHORT_TITLE = 2;
const POSITION_DESCRIPTION = 3;
const POSITION_LEVEL = 4;
const POSITION_METADATA = 5;
const POSITION_MARKETPLACE_PERCENTAGE = 6;

beforeAll(() => {
  init(basePath, port);
});

describe("Test addCardCollection", () => {
  const name = "addCardCollection";
  let Alice;
  let signers;
  let FantastecSwapData;
  let addressMap;
  let code;

  beforeEach(async () => {
    if (internalEmulator) {
      await emulator.start(port, false);
      // load contract
      const name = "FantastecSwapData";
      const to = await getAccountAddress("Alice");
      await deployContractByName({ name, to });
    }

    // set main account
    Alice = await getAccountAddress("Alice");

    // Set transaction signers
    signers = [Alice];

    // Set code
    FantastecSwapData = await getContractAddress("FantastecSwapData");
    addressMap = { FantastecSwapData };
    code = await getTransactionCode({ name, addressMap });
  });

  // Stop emulator, so it could be restarted
  afterEach(async () => {
    if (internalEmulator) return emulator.stop();
  });

  test("a new cardCollection will be added to the contract", async () => {
    // Define arguments
    const args = correctCardCollectionData();

    try {
      const result: TransactionResult = await sendTransaction({ code, signers, args });
      const event: TransactionEvent = result.events[0];
      const data: CardCollection = event.data.item as CardCollection;

      // could be a more fuzzy equality, as isDeactived is a property on CardCollection on chain
      const c1 = cardCollection1;
      expect(data.appId).toBe(c1.appId);
      expect(data.title).toBe(c1.title);
      expect(data.shortTitle).toBe(c1.shortTitle);
      expect(data.description).toBe(c1.description);
      expect(data.level).toBe(c1.level);
      expect(data.metadata).toEqual(c1.metadata);
      expect(data.marketplaceFee).toEqual(c1.marketplaceFee.toFixed(UFIX64_DECIMAL_PLACES));

    } catch (e) {
      console.log(e);
      expect(e).toBeUndefined(); // seems a logical way to elicit an error
    }
  });

  test("checks field appId is valid", async () => {
    // Define arguments
    const argsInvalidAppId = correctCardCollectionData();
    argsInvalidAppId[POSITION_APP_ID][VALUE] = "";
    const args = argsInvalidAppId;

    // Generate addressMap from import statements
    let code = await getTransactionCode({ name, addressMap });

    const fragment = "appId.length != 0";
    await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
  });

  test("checks field title is valid", async () => {
    // Define arguments
    const argsInvalidTitle = correctCardCollectionData();
    argsInvalidTitle[POSITION_TITLE][VALUE] = "";
    const args = argsInvalidTitle;

    // Generate addressMap from import statements
    let code = await getTransactionCode({ name, addressMap });

    const fragment = "title.length != 0";
    await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
  });

  test("checks field shortTitle is valid", async () => {
    // Define arguments
    const argsInvalidShortTitle = correctCardCollectionData();
    argsInvalidShortTitle[POSITION_SHORT_TITLE][VALUE] = "";
    const args = argsInvalidShortTitle;

    // Generate addressMap from import statements
    let code = await getTransactionCode({ name, addressMap });

    const fragment = "shortTitle.length != 0";
    await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
  });

  test("checks field description is valid", async () => {
    // Define arguments
    const argsInvalidDescription = correctCardCollectionData();
    argsInvalidDescription[POSITION_DESCRIPTION][VALUE] = "";
    const args = argsInvalidDescription;

    // Generate addressMap from import statements
    let code = await getTransactionCode({ name, addressMap });

    const fragment = "description.length != 0";
    await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
  });

  test("checks field level is valid", async () => {
    // Define arguments
    const argsInvalidLevel = correctCardCollectionData();
    argsInvalidLevel[POSITION_LEVEL][VALUE] = "";
    const args = argsInvalidLevel;

    const fragment = "level.length != 0";
    await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
  });

  test("check field marketplaceFee cannot go above 100", async () => {
    // Define arguments
    const args = correctCardCollectionData();
    args[POSITION_MARKETPLACE_PERCENTAGE][VALUE] = '100.01';

    const fragment = "marketplaceFee <= 100.0";
    await expect(sendTransaction({ code, signers, args })).rejects.toContain(fragment);
  });

  // describe("that the IPORoyalty array have one entry", () => {
  //   let result: TransactionResult;

  //   beforeAll( async () => {
  //     const args = correctCardCollectionData();
  //     let code = await getTransactionCode({ name, addressMap });
  //     result = await sendTransaction({ code, signers, args });
  //     const event: TransactionEvent = result.events[0];
  //     const data: CardCollection = event.data.item as CardCollection;

  //   });

  //   it("has one entry", async () => {
  //     await expect(1).toBe(1);

  //   });
    // it("which is 100%", async () => {});
    // it("with Address(0x01)", async () => {});
    // it("and is the default", async () => {});
  // });
});
