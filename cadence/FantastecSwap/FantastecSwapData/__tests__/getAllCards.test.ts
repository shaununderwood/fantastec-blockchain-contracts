import * as path from 'path';
import * as t from "@onflow/types";
import {
  init,
  sendTransaction,
  deployContractByName,
  getTransactionCode,
  // emulator,
  getContractAddress,
  getAccountAddress,
  getScriptCode,
  executeScript,
} from "flow-js-testing/dist";
import { TransactionEvent, TransactionResult, CardCollection, Card } from "../../shared/interfaces";
import { card2, correctCardCollectionData, correctCardData } from '../../shared/sharedAssets';

import emulator from "../../shared/emulatorWithNoLimit";

const basePath = path.resolve(__dirname, "../../FantastecSwapData");
const port = 8080;
const internalEmulator = true; // TODO make env

beforeAll(() => {
  init(basePath, port);
});

describe("Test getAllCards", () => {
  let Alice;
  let signers;
  let FantastecSwapData;
  let addressMap;
  let cardId;

  beforeEach(async () => {
    if (internalEmulator) {
      await emulator.start(port, false);
      // load contract
      const name = "FantastecSwapData";
      const to = await getAccountAddress("Alice");
      await deployContractByName({ name, to });
    }

    // set main account
    Alice = await getAccountAddress("Alice");

    // Set transaction signers
    signers = [Alice];

    FantastecSwapData = await getContractAddress("FantastecSwapData");
    addressMap = { FantastecSwapData };

    // add CardCollection for this card
    const name1 = "addCardCollection";
    const code1 = await getTransactionCode({ name: name1, addressMap });
    const args1 = correctCardCollectionData();
    const result1: TransactionResult = await sendTransaction({ code: code1, signers, args: args1 });
    const event1: TransactionEvent = result1.events[0];
    const data1: CardCollection = event1.data.item as CardCollection;
  });

  // Stop emulator, so it could be restarted
  afterEach(async () => {
    if (internalEmulator) return emulator.stop();
  });

  describe("when getting all cards, ", () => {
    let cards;
    let cardId1;
    let cardId2;
    beforeEach(async () => {
      // Add a Card1 to be tested
      const args1 = correctCardData();
      const name1 = "addCard";
      const code1 = await getTransactionCode({ name: name1, addressMap });
      const result1: TransactionResult = await sendTransaction({ code: code1, signers, args: args1 });
      const event1: TransactionEvent = result1.events[0];
      const id1  = (event1.data.item as Card).id;
      expect(result1.status).toBe(4);
      cardId1 = id1;

      // Add a Card to be tested
      const args2 = correctCardData(card2);
      const name2 = "addCard";
      const code2 = await getTransactionCode({ name: name2, addressMap });
      const result2: TransactionResult = await sendTransaction({ code: code2, signers, args: args2 });
      const event2: TransactionEvent = result2.events[0];
      const id2  = (event2.data.item as Card).id;
      expect(result2.status).toBe(4);
      cardId2 = id2;
    });

    it("should return all cards", async () => {
      const name = "getAllCards";
      const code = await getScriptCode({ name, addressMap });
      await expect(executeScript({ code })).resolves.toHaveLength(2);
    });

    it("should not include deactivated cards", async () => {
      const name1 = "deactivateCardById";
      const code1 = await getTransactionCode({ name: name1, addressMap });
      const args1 = [[cardId1, t.UInt64]];
      await expect(sendTransaction({ code: code1, signers, args: args1 })).resolves.toBeTruthy();

      const name2 = "getAllCards";
      const code2 = await getScriptCode({ name: name2, addressMap });
      await expect(executeScript({ code: code2 })).resolves.toHaveLength(1);
    });

    it("should return an empry array if no active cards available", async () => {
      const name1 = "deactivateCardById";
      const code1 = await getTransactionCode({ name: name1, addressMap });
      const args1 = [[cardId1, t.UInt64]];
      await expect(sendTransaction({ code: code1, signers, args: args1 })).resolves.toBeTruthy();
      const name2 = "deactivateCardById";
      const code2 = await getTransactionCode({ name: name2, addressMap });
      const args2 = [[cardId2, t.UInt64]];
      await expect(sendTransaction({ code: code2, signers, args: args2 })).resolves.toBeTruthy();

      const name3 = "getAllCards";
      const code3 = await getScriptCode({ name: name3, addressMap });
      await expect(executeScript({ code: code3 })).resolves.toHaveLength(0);
    });
  });
});
