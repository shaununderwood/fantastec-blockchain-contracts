import * as path from 'path';
import * as t from "@onflow/types";
import {
  init,
  sendTransaction,
  deployContractByName,
  getTransactionCode,
  // emulator,
  getContractAddress,
  getAccountAddress,
  getScriptCode,
  executeScript,
} from "flow-js-testing/dist";
import { TransactionEvent, TransactionResult, CardCollection } from "../../shared/interfaces";
import { correctCardCollectionData } from '../../shared/sharedAssets';

import emulator from "../../shared/emulatorWithNoLimit";

const basePath = path.resolve(__dirname, "../../FantastecSwapData");
const port = 8080;
const internalEmulator = true; // TODO make env

beforeAll(() => {
  init(basePath, port);
});

describe("Test isDeactivated and deactivateCardCollectionById", () => {
  const nameAddCardCollection = "addCardCollection";
  const nameDeactivateCardCollectionById = "deactivateCardCollectionById";
  const nameGetAllCardCollections = "getAllCardCollections";
  let Alice;
  let signers;
  let FantastecSwapData;
  let addressMap;
  let testCardCollectionId;

  beforeEach(async () => {
    if (internalEmulator) {
      await emulator.start(port, false);
      // load contract
      const name = "FantastecSwapData";
      const to = await getAccountAddress("Alice");
      await deployContractByName({ name, to });
    }

    // set main account
    Alice = await getAccountAddress("Alice");

    // Set transaction signers
    signers = [Alice];

    FantastecSwapData = await getContractAddress("FantastecSwapData");
    addressMap = { FantastecSwapData };

    // Add a CardCollection to be deactivated
    let args = correctCardCollectionData();
    let code = await getTransactionCode({ name: nameAddCardCollection, addressMap });
    const resultAddCollection: TransactionResult = await sendTransaction({ code, signers, args });
    const eventAddCollection: TransactionEvent = resultAddCollection.events[0];
    const { id }: CardCollection = eventAddCollection.data.item as CardCollection;
    expect(resultAddCollection?.status).toBe(4);
    testCardCollectionId = id;
  });

  // Stop emulator, so it could be restarted
  afterEach(async () => {
    if (internalEmulator) return emulator.stop();
  });

  describe("after deactivating a CardCollection", () => {
    beforeEach(async () => {
      // deactivate CardCollection
      const name = nameDeactivateCardCollectionById;
      const code = await getTransactionCode({ name, addressMap });
      const args = [[testCardCollectionId, t.UInt64]];
      await expect(sendTransaction({ code, signers, args })).resolves.toHaveProperty("status", 4);
    });

    it("should be available by direct requesting using it's id", async () => {
      // get CardCollection and check it's deactivated
      const name = "getCardCollectionById";
      const code = await getScriptCode({ name, addressMap });
      const args = [[testCardCollectionId, t.UInt64]];
      await expect(executeScript({ code, args })).resolves.toHaveProperty("isDeactivated", true);
    });

    it("should not be available when getting all cardCollections", async () => {
      // ensure CardCollection is not available in getAllCardCollections
      const name = nameGetAllCardCollections;
      const code = await getScriptCode({ name, addressMap });
      const args = [];
      await expect(executeScript({ code, args })).resolves.toHaveLength(0);
    });
  });
});
