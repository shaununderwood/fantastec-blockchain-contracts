import * as t from "@onflow/types";
import { CardCollection, Card } from "./interfaces";
import { makeId } from "../utils";

export const cardCollection1: CardCollection = {
  appId: "SWAP",
  title: "title1",
  shortTitle: "shortTitle1",
  description: "description1",
  level: "level1",
  metadata: { "field1": "value1" },
  marketplaceFee: 6.00,
  // ipos: [{ "percentage": "50.0", "address": "0x01", "default": true }],
};

export const cardCollection2: CardCollection = {
  appId: "SWAP",
  title: "title2",
  shortTitle: "shortTitle2",
  description: "description2",
  level: "level2",
  metadata: { "field2": "value2" },
  marketplaceFee: 5.00,
  // ipos: [
  //   { "percentage": "51.0", "address": "0x01", "default": true },
  //   { "percentage": "49.0", "address": "0x2", "default": false },
  // ],
};

export const correctCardCollectionData = (c: CardCollection = cardCollection1) => {
return [
  ['' + c.appId, t.String],
  ['' + c.title, t.String],
  ['' + c.shortTitle, t.String],
  ['' + c.description, t.String],
  ['' + c.level, t.String],
  [objectToKeyValueArray(c.metadata), t.Dictionary({ key: t.String, value: t.String })],
  ['' + c.marketplaceFee.toFixed(UFIX64_DECIMAL_PLACES), t.UFix64],
  // [arrayOfObjectsToKeyValueArray(c.ipos), t.Array( t.Dictionary([
  //   // { "percentage": 50.0, "address": "0x01", "default": true }
  //   { key: t.String, value: t.UFix64 },
  //   { key: t.String, value: t.Address },
  //   { key: t.String, value: t.Bool },
  // ]))],
]
};

// note, generated hash using https://md5file.com/calculator
export const card1: Card = {
  name: "card 1",
  level: "level1",
  cardType: "PLAYER",
  aspectRatio: "PORTRAIT",
  metadata: { "field1": "value1" },
  mediaData: JSON.stringify([
    {
      type: "type",
      mediaURL: "https://fantastec.io/assets/images/fantastec_swap-logo.png",
      hashAlgo: "SHA256",
      hash: "b4db014f89e24a20fdc6b5170374bbf37f5f8bb7c29aecb72fd8921898ea85ea"
    },
  ]),
  cardCollectionId: 1,
};
export const card2: Card = {
  name: "card 2",
  level: "level 2",
  cardType: "cardType 2",
  aspectRatio: "LINEUP",
  metadata: { "field2": "value2" },
  mediaData: JSON.stringify([
    {
      type: "type",
      mediaURL: "https://fantastec.io/assets/images/fantastec_swap-logo.png",
      hashAlgo: "SHA256",
      hash: "b4db014f89e24a20fdc6b5170374bbf37f5f8bb7c29aecb72fd8921898ea85ea"
    },
    {
      type: "type",
      mediaURL: "https://fantastec.io/assets/images/Phones.png",
      hashAlgo: "SHA256",
      hash: "e0ac7906ba09446d07f20ccbf86802b355b017f8b59318253ba4de3ff1df812b"
    },

  ]),
  cardCollectionId: 1,
};

export const correctCardData = (c: Card = card1) => [
  ['' + c.name, t.String],
  ['' + c.level, t.String],
  ['' + c.cardType, t.String],
  ['' + c.aspectRatio, t.String],
  [objectToKeyValueArray(c.metadata), t.Dictionary({ key: t.String, value: t.String })],
  // // [c.mediaData, t.Array(t.String)]
  ['' + c.mediaData, t.String],
  [c.cardCollectionId, t.UInt64],
];

export function objectToKeyValueArray(object) {
  const result = Object.keys(object).map(field => ({ key: field, value: object[field] }));
  return result;
}

export function arrayOfObjectsToKeyValueArray(array: any[]) {
  const result = array.map(object=>objectToKeyValueArray(object));
  console.log('result', result);
  return result;
}


export const UFIX64_DECIMAL_PLACES: number = 8; // eg '1.00000000' is 1FUSD
