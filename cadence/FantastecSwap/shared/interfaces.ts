
export interface TransactionEvent {
  "type": string, //"A.01cf0e2f2f715450.FantastecSwapData.CardCollectionCreated",
  "transactionId": string, //"21f479ea13f6a2667d7b8de1e3dfaa0e186044dc65dbaff48079b48f259fcca1",
  "transactionIndex": number, //1,
  "eventIndex": number, //0,
  "data": {
    "item": {}
  }
}

export interface TransactionResult {
  "status": number, // 4 - seems to mean OK,
  "statusCode": number, // 0 - non-zero is an error
  "errorMessage": string, //"" - will contain text of the error
  "events": TransactionEvent[]
}

export interface IPORoyalty {
  address: string,
  percentage: string, // UFix64
  default?: boolean,
}
export interface CardCollection {
  appId: string,
  title: string,
  shortTitle: string,
  description: string,
  level: string,
  metadata: {},
  id?: number,
  isDeactivated?: boolean,
  marketplaceFee: number,
  // ipos: IPORoyalty[],
}

export interface Card {
  name: string,
  level: string,
  cardType: string,
  aspectRatio: string,
  metadata: {},
  // mediaData: MediaType[],
  mediaData: string,
  cardCollectionId: number,
  id?: number,
  isDeactivated?: boolean,
}

export interface MediaType {
  type: string,
  mediaURL: string
  hashAlgo: string
  hash: string
}
