
* Should events be changed back to emiting all fields rather than `item`?
* I think `cardData` should be changed to `cardDataById`, and with `cardCollectionData`
* when cardById[card.id]=card and cardByFantastecId[card.fantastecId]=card, are references stored or copies of the card object?  I've a sneaky feeling its a copy. Implications -
  * doubles contract size - is that a problem?
  * potential for either to become imbalanced?
  * maybe we forgo fantastecId on chain, and lookup in db by id?
* tests don't test the difference between panic and pre/post condition error, if this a problem?
* What's the diffrence between panic and pre/post condition error?
* id and fantastecId are both prinary keys.  Might there be a time when the fantastecId needs to change, or the id needs to change?  Eg we deactivate a card based on id, is there ever a time we need to add a new card with a deactivated fanatsecId?
