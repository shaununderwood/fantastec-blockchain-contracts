import ShotzNFT from "../../contracts/ShotzNFT.cdc"

// This transaction configures an account to hold ShotzNFTs.

transaction {
    prepare(signer: AuthAccount) {
        // if the account doesn't already have a collection
        if signer.borrow<&ShotzNFT.Collection>(from: ShotzNFT.CollectionStoragePath) == nil {

            // create a new empty collection
            let collection <- ShotzNFT.createEmptyCollection()
            
            // save it to the account
            signer.save(<-collection, to: ShotzNFT.CollectionStoragePath)

            // create a public capability for the collection
            signer.link<&ShotzNFT.Collection{ShotzNFT.ShotzNFTCollectionPublic}>(ShotzNFT.CollectionPublicPath, target: ShotzNFT.CollectionStoragePath)
        }
    }
}
