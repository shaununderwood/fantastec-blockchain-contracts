import NonFungibleToken from "../../standard/contracts/NonFungibleToken.cdc"

// ShotzNFT
// NFT items for Fantastec Swap!
//
pub contract ShotzNFT: NonFungibleToken {

    // TODO Capabilities Required
    // Owner can update the mediaURL, but not the media hash, allowing the owner to move the NFT's resource to another location
    // Admin can update all attributed whilst is it not sealed

    // Events
    pub event ContractInitialized()
    pub event Withdrawn(id: UInt64, from: Address?)
    pub event Deposited(id: UInt64, to: Address?)
    pub event Minted(id: UInt64, typeID: UInt64)
    // proposed Events
    // pub event UpdatedByAdmin
    // pub event UpdatedByOwner

    // Named Paths
    pub let CollectionStoragePath: StoragePath
    pub let CollectionPublicPath: PublicPath
    pub let MinterStoragePath: StoragePath

    // The total number of ShotzNFT that have been minted
    // QUESTION: is this relevant?
    pub var totalSupply: UInt64

    // ShotzNFT
    // QUESTION: can all these extra fields be made into a struct ShotzNFTMetadata?
    pub resource NFT: NonFungibleToken.INFT {
        pub let id: UInt64
        pub let mintNumber: UInt64
        pub let totalSupply: UInt64 // QUESTION: is this needed?
        pub let mediaURL: String?
        pub let mediaHash: String?
        pub let cardId: UInt64
        pub let albumId: UInt64?
        pub let clubId: UInt64?
        pub let levelId: UInt64?
        pub let collectionId: UInt64?

        // initializer
        //
        init(initID: UInt64, metadata: CardMetadata, offset: UInt64) {
            // QUESTION: IS THIS CONSIDERED, BY CONVENTION, THE MINT NUMBER OF THE COLLECTIBLE
            // OUR ANSWER: id is _JUST_ a indicator of the total number of NFTs minted by fantastec
            // each NFT must have a mint number which increase on the previous minted NFT by 1
            // collectionTotalSupply(?) contains 
            self.id = initID 
            self.metatdata = metadata
            // Collection<-(get that collection(metadata.collection_id))
            // Collection.sealed? return: continue
            // Collection.totalSupply != Collection.currentMintNumbef? return: continue
            Collection.collectionTotalSupply[`${metadata.collection_id}-${metadata.edition_id}`]++
            self.mintNumber = collectionTotalSupply[metadata.collection_id]
        }
    }
    // Resource Collection
    // Collection

    // This is the interface that users can cast their ShotzNFT Collection as
    // to allow others to deposit ShotzNFT into their Collection. It also allows for reading
    // the details of ShotzNFT in the Collection.
    pub resource interface ShotzNFTCollectionPublic {
        pub fun getIds(): [UInt64]
        pub fun deposit(token: @NonFungibleToken.NFT)
          // QUESTION: should token be @ShotzNFT.NFT?

          // TODO: TEST
          pre {
            (ownedNFTs[token.id] != nil):
              "Cannot deposit token as id already exists"
          }
          // TODO: TEST as cadence supports ~100k entries per dictionary
          post {
            (ownedNFTs[token.id].id != token.id):
              panic("Failed to add NFT")
          }
        pub fun borrowNFT(id: UInt64): &NonFungibleToken.NFT {
            post {
                (result == nil) || (result?.id == id):
                    "Cannot borrow NonFungibleToken reference: The ID of the returned reference is unknown"
            }
        }
        pub fun borrowShotzNFT(id: UInt64): &ShotzNFT.NFT? {
            post {
                (result == nil) || (result?.id == id):
                    "Cannot borrow ShotzNFT reference: The ID of the returned reference is unknown"
            }
        }
        // TODO: TEST - Admin Function
        pub fun depositReplace(token: @NonFungibleToken.NFT) {
            post {
              (ownedNFTs[token.id].id != token.id):
                panic("Failed to add NFT")
            }
        }
        // QUESTION: Might these be useful? implement as needed
        // pub fun getIdsByPropertyValue(property: String, value: String): [UInt64]
        // pub fun getIdsByCollection(collectionId: UInt64): [UInt64]
        // pub fun getIdsByAlbumId(albumId: UInt64): [UInt64]
        // pub fun getIdsByLevelId(levelId: UInt64): [UInt64]
        // pub fun getIdsByCardId(cardId: UInt64): [UInt64]
        // pub fun getIdsByMediaHash(mediaHash: String): [UInt64]
        // pub fun getIdsByMediaURL(mediaURL: String): [UInt64]
        // pub fun getIdsCollectionStats(): JSONString
    }

    // Collection
    // A collection of ShotzNFT owned by an account
    pub resource Collection: ShotzNFTCollectionPublic, NonFungibleToken.Provider, NonFungibleToken.Receiver, NonFungibleToken.CollectionPublic {
        // dictionary of NFT conforming tokens
        // NFT is a resource type with an `UInt64` ID field
        pub var ownedNFTs: @{UInt64: NonFungibleToken.NFT}

        // withdraw
        // Removes an NFT from the collection and moves it to the caller
        pub fun withdraw(withdrawId: UInt64): @NonFungibleToken.NFT {
            // QUESTION: should this be part of the NFT?  An on-chain-lock seems reasonable, but what are cost implications?
            // QUESTION: should this be a pre condition?
            if self.ownedNFTs[withdrawId]?.locked ?? false panic("Cannot withdraw, NFT is locked")

            let token <- self.ownedNFTs.remove(key: withdrawID) ?? panic("missing NFT")
            emit Withdrawn(id: token.id, from: self.owner?.address)
            return <-token
        }

        // deposit
        // Takes a NFT and adds it to the collections dictionary
        // and adds the ID to the id array
        // QUESTION: shouldn't @NonFungibleToken.NFT be @ShotzNFT.NFT?
        // QUESTION: should we disallow deposit if id already exists?
        pub fun deposit(token: @NonFungibleToken.NFT) {
            let token <- token as! @ShotzNFT.NFT
            let id: UInt64 = token.id

            // add the new token to the dictionary which removes the old one
            let oldToken <- self.ownedNFTs[id] <- token

            emit Deposited(id: id, to: self.owner?.address)

            // QUESTION: if the deposited is the same as the existsing, did we just destroyed the existsing?
            destroy oldToken
        }

        // getIds
        // Returns an array of the IDs that are in the collection
        pub fun getIds(): [UInt64] {
            return self.ownedNFTs.keys
        }

        // borrowNFT
        // Gets a reference to an NFT in the collection
        // so that the caller can read its metadata and call its methods
        //
        // QUESTION: should NonFungibleToken.NFT by ShotzNFT?
        pub fun borrowNFT(id: UInt64): &NonFungibleToken.NFT {
            return &self.ownedNFTs[id] as &NonFungibleToken.NFT
        }

        // borrowShotzNFT
        // Gets a reference to an NFT in the collection as a ShotzNFT,
        // exposing all of its fields (including the typeID).
        // This is safe as there are no functions that can be called on the ShotzNFT.
        //
        pub fun borrowShotzNFT(id: UInt64): &ShotzNFT.NFT? {
            if self.ownedNFTs[id] != nil {
                let ref = &self.ownedNFTs[id] as auth &NonFungibleToken.NFT
                return ref as! &ShotzNFT.NFT
            } else {
                return nil
            }
        }

        // destructor
        destroy() {
            destroy self.ownedNFTs
        }

        // initializer
        //
        init () {
            self.ownedNFTs <- {}
        }
    }

    // createEmptyCollection
    // public function that anyone can call to create a new empty collection
    //
    // QUESTION: Why not to return ShotszNFT.Collection?
    pub fun createEmptyCollection(): @NonFungibleToken.Collection {
        return <- create Collection()
    }

    // NFTMinter
    // Resource that an admin or something similar would own to be
    // able to mint new NFTs
    //
	pub resource NFTMinter {

		// mintNFT
    // Mints a new NFT with a new id
		// and deposit it in the recipients collection using their collection reference
    //
    // QUESTION: we should have a bulk minter to create thousands of NFTs at once, which would set the mintNumber and totalSupply automatically
		pub fun mintNFT(
      recipient: &{NonFungibleToken.CollectionPublic}, 
      mintNumber: UInt64,
      totalSupply: UInt64,
      mediaURL: String,
      mediaHash: String,
      cardId: UInt64, // passed in via metadata on FantastecSwap
      AlbumId: UInt64, // passed in via metadata on FantastecSwap
      clubId: UInt64,
      levelId: UInt64,
      collectionId: UInt64,

      mediaData: [ // NEEDED!
        {type: "video", mediaUrl: "", hash: ""}
        {type: "paralax_layer_1", mediaUrl: "", hash: ""}
      ]

    ) {
      var id = ShotzNFT.totalSupply
			// deposit it in the recipient's account using their reference
      var newShotzNFT <- create ShotzNFT.NFT(
        id: id,
        mintNumber: metadata.mintNumber,
        totalSupply: totalSupply,
        mediaURL: mediaURL,
        mediaHash: mediaHash,
        cardId: cardId,
        AlbumId: AlbumId,
        clubId: clubId,
        levelId: levelId,
        collectionId: collectionId,
      )
      // QUESTION: Why is emit called BEFORE depositing the token?
      emit Minted(id: newShotzNFT.id)
      ShotzNFT.totalSupply = ShotzNFT.totalSupply + (1 as UInt64)
			recipient.deposit(token: <-newShotzNFT)
		}
	}

    // fetch
    // Get a reference to a KittyItem from an account's Collection, if available.
    // If an account does not have a ShotzNFT.Collection, panic.
    // If it has a collection but does not contain the itemID, return nil.
    // If it has a collection and that collection contains the itemID, return a reference to that.
    //
    pub fun fetch(_ from: Address, id: UInt64): &ShotzNFT.NFT? {
        let collection = getAccount(from)
            .getCapability(ShotzNFT.CollectionPublicPath)!
            .borrow<&ShotzNFT.Collection{ShotzNFT.ShotzNFTCollectionPublic}>()
            ?? panic("Couldn't get collection")
        // We trust ShotzNFT.Collection.borrowShotzNFT to get the correct itemId
        return collection.borrowShotzNFT(id: id)
    } 

    // initializer
    //
	init() {
        // Set our named paths  
        // TODO: REMOVE SUFFIX (001) BEFORE RELEASE
        self.CollectionPublicPath = /public/ShotzNFTCollection001
        self.MinterStoragePath = /storage/ShotzNFTMinter001
        self.CollectionStoragePath = /storage/ShotzNFTCollection001

        // Initialize the total supply
        self.totalSupply = 0

        // Create a Minter resource and save it to storage
        let minter <- create NFTMinter()
        self.account.save(<-minter, to: self.MinterStoragePath)

        emit ContractInitialized()
	}
}
 