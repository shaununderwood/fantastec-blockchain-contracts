import FantastecSwap from 0x01

transaction {

  var admin: &FantastecSwap.Admin

  prepare(acct: AuthAccount) {
    self.admin = acct.borrow<&FantastecSwap.Admin>(from: FantastecSwap.AdminStoragePath)
      ?? panic("No Admin found on account")
  }

  execute {
    let id = 1 as UInt64
    let fantastecId = "12345678901234567890123456789000"
    let appId = "SWAP"
    let title = "Test Collection 2"
    let shortTitle = "Collection 2"
    let description = "Basic test collection"
    let level = "AMBER"

    let cardCollection: FantastecSwap.CardCollectionData = self.admin.updateCardCollectionById(
      id: id,
      fantastecId: fantastecId,
      appId: appId,
      title: title,
      shortTitle: shortTitle,
      description: description,
      level: level,
    )
    log("CardCollection updated, id: ".concat(cardCollection.id.toString()))
  }
}