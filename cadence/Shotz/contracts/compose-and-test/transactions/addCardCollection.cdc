import FantastecSwap from 0x01

transaction {

  var admin: &FantastecSwap.Admin

  prepare(acct: AuthAccount) {
    self.admin = acct.borrow<&FantastecSwap.Admin>(from: FantastecSwap.AdminStoragePath)
      ?? panic("No Admin found on account")
  }

  execute {
    let cardCollection: FantastecSwap.CardCollectionData = self.admin.addCardCollection(
      fantastecId: "12345678901234567890123456789000",
      appId: "SWAP",
      title: "Test Collection 1",
      shortTitle: "Collection 1",
      description: "Basic test collection",
      level: "AMBER",
    )
    log("CardCollection added, id: ".concat(cardCollection.id.toString()))
  }
}