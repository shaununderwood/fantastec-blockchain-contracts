import FantastecSwap from 0x01

transaction {

  var admin: &FantastecSwap.Admin

  prepare(acct: AuthAccount) {
    self.admin = acct.borrow<&FantastecSwap.Admin>(from: FantastecSwap.AdminStoragePath)
      ?? panic("No Admin found on account")
  }

  execute {
    let id = 1 as UInt64
    let fantastecId = "12345678901234567890123456789012"
    let name = "shaun1"
    let level = "bronze"
    let cardType = "person"
    let metadata = {"field": "value"}

    let card: FantastecSwap.CardData = self.admin.updateCardById(
      id: id,
      fantastecId: fantastecId,
      name: name,
      level: level,
      cardType: cardType, 
      metadata: metadata
    )

    log("Card updated, id: ".concat(card.id.toString()))
  }
}