import FantastecSwap from 0x01

transaction {

  var admin: &FantastecSwap.Admin

  prepare(acct: AuthAccount) {
    self.admin = acct.borrow<&FantastecSwap.Admin>(from: FantastecSwap.AdminStoragePath)
      ?? panic("No Admin found on account")
  }

  execute {
    let card: FantastecSwap.CardData = self.admin.addCard(
      fantastecId: "12345678901234567890123456789012",
      name: "shaun2",
      level: "goldspecial", 
      cardType: "person", 
      metadata: {"field":"value"}
    )
    log("Card added, id: ".concat(card.id.toString()))
  }
}