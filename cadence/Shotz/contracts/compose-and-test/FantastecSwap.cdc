
// import NonFungibleToken from 0x02 // 0xNonFungibleToken // "./NonFungibleToken.cdc"

pub contract FantastecSwap {
  // QUESTION:
  //  I dislike how cluttered the contract gets with soo many resources and structs etc.
  //  Can we be more typical and extract resources and structs to separate contracts?

  // Contract Events
  pub event ContractInitialized()
  
  // TODO: I think we can replace all these fields with pub event Card<Event>(card: CardData)

  // Card emits itself in events to allow the Event Processor to update Fantastec systems
  pub event CardCreated(id: UInt64, fantastecId: String, name: String, level: String, cardType: String, aspectRatio: String, metadata: {String: String}, mediaData: [MediaType], cardCollectionId: UInt64, editionMintVolume: {UInt64: UInt64})
  pub event CardUpdated(id: UInt64, fantastecId: String, name: String, level: String, cardType: String, aspectRatio: String, metadata: {String: String}, mediaData: [MediaType], cardCollectionId: UInt64, editionMintVolume: {UInt64: UInt64})
  
  // CardCollection emits itself in events to allow the Event Processor to update Fantastec systems
  pub event CardCollectionCreated(id: UInt64, title: String, shortTitle: String, description: String, level: String, metadata: {String: String})
  pub event CardCollectionUpdated(id: UInt64, title: String, shortTitle: String, description: String, level: String, metadata: {String: String})

  // NFTMinter emits the NFT in events to allow the Event Processor to update Fantastec systems
  pub event NFTMinterCreated()
  pub event NFTMinted(id: UInt64, cardId: UInt64, cardCollectionId: UInt64, edition: UInt64, mintNumber: UInt64)
  pub event NFTDestroyed(id: UInt64, cardId: UInt64)

  /* 
    All Contract resident data should not be directly accessible from outside the contract.
    Instead, these data are private and accessible via accessor routines.
  */
  access(self) var cardsData: {UInt64: CardData}
  access(self) var cardsDataByFantastecId: {String: CardData} // reference to cardsData held object
  access(self) var nextCardId: UInt64

  access(self) var cardCollectionData: {UInt64: CardCollectionData}
  access(self) var cardCollectionDataByFantastecId: {String: CardCollectionData}
  access(self) var nextCardCollectionId: UInt64

  // total number of NFTs ever printed by this contract
  pub var totalSupply: UInt64

  pub let AdminStoragePath: StoragePath
  // pub let MinterStoragePath: StoragePath
  // pub let CollectionStoragePath: StoragePath
  // pub let CollectionPublicPath: PublicPath

  /*
    Fantastec Swap's system is based around Cards and Collections.
    
    A CardCollection
      * the term Collection is already used to describe a resource holding resources, hence CardCollection
      * describes the theme of the containing Cards, as well as collection level information.
      * may be associated with 1 or more Cards.
      * the minter mints against

    A Card 
      * describes an NFT that make up a collection
      * may be used to mint 1 or more NFTs

  */

  /* MediaType shape of a media item associated with a Card */
  access(all) struct MediaType {
    pub let type: String      // currently ["PLAYER","LINEUP"]
    pub let mediaURL: String  // eg "https://s3bucket.aws.com/path/to/image/jpg" or "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=="
    pub let hashAlgo: String  // eg SHA256
    pub let hash: String      // eg 40a2ef27ec35668ac2dbe5b182f6d5159ee94b3d7fa5f40fad18ccbe423f8afd or 27c960a2b437053a3714ec7ce4a074a96fa4d134f58c554cdd221a4722811fdc
    init(type: String, mediaURL: String, hashAlgo: String, hash: String){
      self.type = type
      self.mediaURL = mediaURL
      self.hashAlgo = hashAlgo
      self.hash = hash
    }
  }

  pub struct CardData {
    pub var id: UInt64                  // a unique sequential number
    pub var name: String                // Card name
    pub var level: String               // Level, one of 15 different levels
    pub var cardType: String            // Currently either [Player, Lineup]??
    pub var aspectRatio: String         // ??
    pub var metadata: {String: String}  // a data structure determined by cardType
    pub var mediaData: [MediaType]      // All media for an NFT is held in a mediaData array
                                        // An NFT should have at least 1 mediaData entry being an image representing the NFT
    pub var cardCollectionId: UInt64    // The id of the collection to which this card belongs
    pub var fantastecId: String         // internal unique id used by Fantastec's CMS

    // The desired number of NFTs to mint per edition.
    // Admin may add a new entry to editionMintVolume, but not change any entries already present.
    // All other users have READONLY access to this property.
    // For example, {1:70} means 1st edition, 70 cards minted.
    // TODO: function to implement this
    pub var editionMintVolume: {UInt64: UInt64}

    init(
      fantastecId: String,
      name: String, 
      level: String, 
      cardType: String,
      aspectRatio: String,
      metadata: {String: String},
      mediaData: [MediaType],
      cardCollectionId: UInt64,
      id: UInt64?,                      // if nil, nextCardId is used
      ){
      pre {
        fantastecId.length == 32: "fantastecId must be 32 characters in length"
        name.length > 0: "name cannot be empty"
        level.length > 0: "level cannot be empty"
        cardType.length > 0: "cardType cannot be empty"
        aspectRatio.length >0: "aspectRatio cannot be empty"
        metadata.length > 0: "metadata cannot be empty"
        mediaData.length > 0: "mediaData cannot be empty"
        cardCollectionId > (0 as UInt64): "cardCollectionId must have a value greater than 1"
      }

      self.id = 0 // default value to pass syntax checker
      self.fantastecId = fantastecId
      self.name = name
      self.level = level
      self.cardType = cardType
      self.aspectRatio = aspectRatio
      self.metadata = metadata
      self.mediaData = mediaData
      self.cardCollectionId = cardCollectionId
      self.editionMintVolume = {}

      if (id != nil){
        // Update existing struct
        self.id = id!
        FantastecSwap.cardsData[self.id] = self
        FantastecSwap.cardsDataByFantastecId[self.fantastecId] = self

        emit CardUpdated(
          id: self.id,
          fantastecId: self.fantastecId,
          name: self.name,
          level: self.level,
          cardType: self.cardType,
          aspectRatio: self.aspectRatio,
          metadata: self.metadata,
          mediaData: self.mediaData,
          cardCollectionId: self.cardCollectionId,
          editionMintVolume: self.editionMintVolume
        )
        return
      }

      // ensure fantastecId is not been used
      if (FantastecSwap.cardsDataByFantastecId[fantastecId] != nil){
        panic("cannot create CardsData when fantastecId already in use")
      }

      // Set Card's id, and manage next card id
      self.id = FantastecSwap.nextCardId
      FantastecSwap.nextCardId = FantastecSwap.nextCardId + (1 as UInt64)
      FantastecSwap.cardsData[self.id] = self
      FantastecSwap.cardsDataByFantastecId[self.fantastecId] = self

      emit CardCreated(
          id: self.id,
          fantastecId: self.fantastecId,
          name: self.name,
          level: self.level,
          cardType: self.cardType,
          aspectRatio: self.aspectRatio,
          metadata: self.metadata,
          mediaData: self.mediaData,
          cardCollectionId: self.cardCollectionId,
          editionMintVolume: self.editionMintVolume
      )
    }
  }

  pub struct CardCollectionData {
    pub var id: UInt64
    pub var fantastecId: String
    pub var appId: String
    pub var title: String
    pub var shortTitle: String
    pub var description: String
    pub var level: String
    pub var metadata: {String: String}

    init(
      fantastecId: String, 
      appId: String, 
      title: String, 
      shortTitle: String, 
      description: String, 
      level: String,
      metadata: {String: String},
      id: UInt64?, // if nil, nextCardCollectionId is used
      ){
      pre {
        fantastecId.length == 32: "fantastecId must be 32 characters in length"
        appId.length != 0: "appId cannot be empty"
        title.length != 0: "title cannot be empty"
        shortTitle.length != 0: "shortTitle cannot be empty"
        description.length != 0: "description cannot be empty"
        level.length != 0: "level cannot be empty"
      }
      self.fantastecId = fantastecId
      self.appId = appId
      self.title = title
      self.shortTitle = shortTitle
      self.description = description
      self.level = level
      self.id = FantastecSwap.nextCardCollectionId
      self.metadata = {}

      if (id != nil){
        // Update existing struct
        self.id = id!
        FantastecSwap.cardCollectionData[self.id] = self
        FantastecSwap.cardCollectionDataByFantastecId[self.fantastecId] = self

        emit CardCollectionUpdated(
          id: self.id,
          title: self.title,
          shortTitle: self.shortTitle,
          description: self.description,
          level: self.level,
          metadata: self.metadata,
        )
        return
      }

      // ensure fantastecId is not been used
      if (FantastecSwap.cardCollectionDataByFantastecId[fantastecId] != nil){
        panic("cannot create CardCollectionData when fantastecId already in use")
      }

      // Save new struct
      FantastecSwap.nextCardCollectionId =  FantastecSwap.nextCardCollectionId + (1 as UInt64)
      FantastecSwap.cardCollectionData[self.id] = self
      FantastecSwap.cardCollectionDataByFantastecId[self.fantastecId] = self

      emit CardCollectionCreated(
        id: self.id,
        title: self.title,
        shortTitle: self.shortTitle,
        description: self.description,
        level: self.level,
        metadata: self.metadata,
      )
    }
  }

  // pub struct NFTData {
  //   pub let cardId: UInt64
  //   pub let cardCollectionId: UInt64
  //   pub let edition: UInt64
  //   pub let mintNumber: UInt64
  //   init(cardId: UInt64, cardCollectionId: UInt64, edition: UInt64, mintNumber: UInt64){
  //     self.cardId = cardId
  //     self.cardCollectionId = cardCollectionId
  //     self.edition = edition
  //     self.mintNumber = mintNumber
  //   }
  // }
  // pub resource NFT: NonFungibleToken.INFT {
  //     pub let data: NFTData
  //     pub let id: UInt64

  //     init( cardId: UInt64,
  //           cardCollectionId: UInt64,
  //           edition: UInt64,
  //           mintNumber: UInt64) {

  //         // TODO: media stored on NFT for future updating

  //         // Increment the global IDs
  //         FantastecSwap.totalSupply = FantastecSwap.totalSupply + (1 as UInt64)

  //         self.id = FantastecSwap.totalSupply

  //         // set the metadata struct
  //         self.data = NFTData(
  //           cardId: cardId,
  //           cardCollectionId: cardCollectionId,
  //           edition: edition,
  //           mintNumber: mintNumber
  //         )

  //         // TODO: do we return trhe entire details for Mongo via this event
  //         emit NFTMinted(
  //           id: self.id, 
  //           cardId: self.data.cardId, 
  //           cardCollectionId: self.data.cardCollectionId, 
  //           edition: self.data.edition, 
  //           mintNumber: self.data.mintNumber
  //         )
  //     }

  //     destroy() {
  //         emit NFTDestroyed(id: self.id, cardId: self.id)
  //     }
  // }

  pub resource Admin {
    pub fun addCard(
      fantastecId: String, 
      name: String, 
      level: String, 
      cardType: String, 
      aspectRatio: String, 
      metadata: {String: String},
      mediaData: [MediaType],
      cardCollectionId: UInt64
    ): CardData {
      var newCard: CardData = CardData(
        fantastecId: fantastecId,
        name: name,
        level: level,
        cardType: cardType,
        aspectRatio: aspectRatio,
        metadata: metadata,
        mediaData: mediaData,
        cardCollectionId: cardCollectionId,
        id: nil
      )
      return newCard
    }
    pub fun updateCardById(
      fantastecId: String, 
      name: String, 
      level: String, 
      cardType: String, 
      aspectRatio: String, 
      metadata: {String: String},
      mediaData: [MediaType],
      cardCollectionId: UInt64,
      id: UInt64,
    ): CardData {
      let card: CardData = FantastecSwap.getCardById(id: id)
        ?? panic("Card not found with id: ".concat(id.toString()))

      var updatedCard: CardData = CardData(
        fantastecId: fantastecId,
        name: name,
        level: level,
        cardType: cardType,
        aspectRatio: aspectRatio,
        metadata: metadata,
        mediaData: mediaData,
        cardCollectionId: cardCollectionId,
        id: id
      )
      return updatedCard
    }
    pub fun addEditionMintVolume(cardId: UInt64, edition: UInt64, mintVolume: UInt64){
      pre {
        FantastecSwap.cardsData[cardId] != nil: "no card with that id found in cardsData"
      }
      let card: CardData = FantastecSwap.cardsData[cardId]!
      if (card == nil){
        panic("cannot add or update edition")
      }
      // update edition with maximum number of be minted
      card.editionMintVolume[edition] = mintVolume
    }
    pub fun addCardCollection(
      fantastecId: String, 
      appId: String, 
      title: String, 
      shortTitle: String, 
      description: String, 
      level: String,
      metadata: {String: String},
    ): CardCollectionData {
      var newCardCollection: CardCollectionData = CardCollectionData(
        fantastecId: fantastecId,
        appId: appId,
        title: title,
        shortTitle: shortTitle,
        description: description,
        level: level,
        metadata: metadata,
        id: nil,
      )
      return newCardCollection
    }
    pub fun updateCardCollectionById(
      fantastecId: String, 
      appId: String, 
      title: String, 
      shortTitle: String, 
      description: String, 
      level: String,
      metadata: {String: String},
      id: UInt64,
    ): CardCollectionData {
      let cardCollection: CardCollectionData = FantastecSwap.getCardCollectionById(id: id)
        ?? panic("CardCollection not found with id: ".concat(id.toString()))

      var updatedCardCollection: CardCollectionData = CardCollectionData(
        fantastecId: fantastecId,
        appId: appId,
        title: title,
        shortTitle: shortTitle,
        description: description,
        level: level,
        metadata: metadata,
        id: id,
      )
      return updatedCardCollection
    }
  }

  // pub resource NFTMinter {
  //   var nextMintNumberByEdition: {UInt64: UInt64}
  //   // TODO: The ref implementation has a recipient Address in mintNFTs. Needed?
  //   //        https://github.com/onflow/kitty-items/blob/92f762fabbc679315ca36a8e4507201cb33622d2/cadence/contracts/KittyItems.cdc#L155
  //   /*
  //     Card must have a reference to it's collection
  //     When adding a Card or update a Card, set the cardCollectionId
  //     Add cardCollectionId: UInt64 to the card definition


  //    */
  //   pub fun mintNFT(recipient: &{NonFungibleToken.CollectionPublic}, cardId: UInt64, edition: UInt64) {
  //       let card: CardData = FantastecSwap.cardsData[cardId] ??
  //         panic("Card with cardId not found")

  //       var nextMintNumber: UInt64 = self.nextMintNumberByEdition[edition]!
  //       mintNumber = nextMintNumber 

  //       let newNFT: @NFT <- create NFT( cardId: cardId,
  //                                       cardCollectionId: card.cardCollectionId,
  //                                       edition: edition,
  //                                       mintNumber: mintNumber
  //                                     )
  //       recipient.deposit(token: <- newNFT)
  //       card.editionMintVolume[edition] = mintNumber
  //   }

  //   With Ramp in mind, we can use 2 accounts 1 for FTY andother for NFT
  //      User can could bring their own FT wallet which we link to to authroize transactions

  //   /*
  //     batchMintNFTs is used to create NFTs on-chain within a single transactions, so's minimise costs
  //     when minting NFTs (eg 1 Transaction to 1 NFT = $$ whilst multiple NFTs per transaction is less)
  //    */
  //   pub fun batchMintNFT(cardId: UInt64, batchMintVolume: UInt64, edition: UInt64): @Collection {

  //   }
    
  //   // TODO:  MIGRATION!
  //   //    pub fun disableMigration() { FantastecSwap.migrationComplete = true }
  //   //    pub fun mintMigratedNFT() { pre { FantastecSwap.migrationComplete==false : "migration not possible once migrationComplete is true" }}
  //   //    pub fun batchMintMigratedNFT() { pre { FantastecSwap.migrationComplete==false : "migration not possible once migrationComplete is true" }}

  //   init(){
  //     emit NFTMinterCreated()
  //   }    
  // }


  // Public Card functions
  pub fun getAllCards():[FantastecSwap.CardData]{
    return FantastecSwap.cardsData.values
  }
  pub fun getCardById(id: UInt64): FantastecSwap.CardData? {
    return FantastecSwap.cardsData[id]
  }
  pub fun getCardIds(): [UInt64] {
    return FantastecSwap.cardsData.keys
  }

  // Public CardCollection functions
  pub fun getAllCardCollections():[FantastecSwap.CardCollectionData]{
    return FantastecSwap.cardCollectionData.values
  }
  pub fun getCardCollectionById(id: UInt64): FantastecSwap.CardCollectionData? {
    return FantastecSwap.cardCollectionData[id]
  }
  pub fun getCardCollectionIds(): [UInt64] {
    return FantastecSwap.cardCollectionData.keys
  }

  init() {
    // set storage paths
    self.AdminStoragePath = /storage/FantastecSwapAdmin
    // self.MinterStoragePath = /storage/FantastecSwapMinter

    self.cardsData  = {}
    self.cardsDataByFantastecId  = {}
    self.cardCollectionData = {}
    self.cardCollectionDataByFantastecId  = {}

    self.totalSupply = 0
    self.nextCardId = 1
    self.nextCardCollectionId = 1

    self.account.save<@Admin>(<- create Admin(), to: self.AdminStoragePath)
    // self.account.save<@NFTMinter>(<- create NFTMinter(), to: self.MinterStoragePath)

    emit ContractInitialized()
  }
}