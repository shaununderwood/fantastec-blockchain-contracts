/*
Description: Central smart contract for Fantastec Swap

*/

pub contract FantastecSwap {

  // -----------------------------------------------------------------------
  // Contract Event definitions
  // -----------------------------------------------------------------------

  // Contract
  pub event ContractInitialized()

  // Cards
  // QUESTION: might we have a max mint volume on a card?
  // QUESTION: fantastecId AND id?
  pub event CardCreated(id: UInt64, fantastecId: String, name: String, level: String, cardType: String, mintVolume: UInt64, metadata: {String: String})
  pub event CardUpdated(id: UInt64, keyValues: {String: String})
  pub event CardRemoved(id: UInt64)

  // CardCollections
  pub event CardCollectionCreated(id: UInt64, fantastecId: String, appId: String, title: String, shortTitle: String, description: String, level: String)
  pub event CardCollectionUpdated(id: UInt64, keyValues: {String: String})
  pub event CardCollectionRemoved(id: UInt64)

  // NFTs
  pub event NFTMinted(id: self.id, cardId: self.data.cardId, edition: self.data.edition, mintNumber: self.data.mintNumber)
  pub event NFTDestroyed(id: self.id)

  // -----------------------------------------------------------------------
  // Contract-level fields
  // -----------------------------------------------------------------------

  // total number of unique NFTs minted to date
  // Because NFTs can be destroyed, it doesn't necessarily mean that this
  // reflects the total number of NFTs in existence
  pub var totalSupply: UInt64
  pub var nextCardId: UInt64
  pub var nextCardCollectionId: UInt64

  // variable size dictionary of CardCollection structs
  access(self) var cardCollectionsData: {UInt64: CardCollectionData}
  // variable size dictionary of CardCollection resources
  access(self) var cardCollections: @{UInt64: CardCollection}

  // variable size dictionary of Card structs
  access(self) var cardsData: {UInt64: CardData}
  access(self) var cardsByFantastecId: {String: CardData}
  // variable size dictionary of Card resources
  access(self) var cards: @{UInt64: Card}


  // -----------------------------------------------------------------------
  // Contract-level Composite Type DEFINITIONS
  // -----------------------------------------------------------------------

  // CardData
  // metadata contains all the fields of a card  that are specific to it's type
  // All top-level fields are required by all Card types.
  // The shape of the metadata is determined by the cardType.
  // QUESTION: Should we include a CardTypeData struct?  If so, perhaps in a
  //    different contract to prevent this onctract from being updated repeatedly?
  pub struct CardData {
    pub let id: UInt64
    pub let fantastecId: String // used as part of Card 
    pub let name: String
    pub let level: String
    pub let mintVolume: UInt64
    pub let cardType: String
    pub let metadata: {String: String}

    init(
      fantastecId: String,
      name: String, 
      level: String, 
      cardType: String, 
      metadata: {String: String}
      ){
      pre {
        fantastecId.length != 32: "fantastecId must be 32 characters in length"
        name.length != 0: "name cannot be empty"
        level.length != 0: "level cannot be empty"
        cardType.length != 0: "cardType cannot be empty"
        metadata.length != 0: "metadata cannot be empty"
      }

      self.fantastecId = fantastecId
      self.name = name
      self.level = level
      self.cardType = cardType
      self.metadata = metadata

      self.id = FantastecSwap.nextCardId
      self.mintedVolume = 0

      FantastecSwap.nextCardId =  FantastecSwap.nextCardId + (1 as UInt64)
      
      // QUESTION: Surely its the resource that emits this event?
      emit CardCreated(
        id: self.id,
        fantastecId: self.fantastecId,
        name: self.name,
        level: self.level,
        cardType: self.cardType,
        metadata: self.metadata
      )
    }
  }

  // Admin is a special authorization resource that 
  // allows the owner to perform important functions to modify the 
  // various aspects of the cards, swap collections, and nfts
  //
  pub resource Admin {
    pub fun createNewAdmin(): @Admin {
        return <-create Admin()
    }
    pub fun createCard(
      fantastecId: String,
      name: String, 
      level: String, 
      cardType: String, 
      metadata: {String: String}
    ): UInt64 {
      var newCard = CardData(
        fantastecId: fantastecId,
        name: name,
        level: level,
        cardType: cardType,
        metadata: metadata
      )

      FantastecSwap.cardsData[newCard.id]
      return newCard.id
    }
  }


  // CardCollectionData
  pub struct CardCollectionData {
    pub let id: UInt64
    pub let fantastecId: String
    pub let appId: String
    pub let title: String
    pub let shortTitle: String
    pub let description: String
    pub let level: String

    init(
      fantastecId: String, 
      appId: String, 
      title: String, 
      shortTitle: String, 
      description: String, 
      level: String
      ){
      pre {
        fantastecId.length != 32: "fantastecId must  be 32 characters in length"
        appId.length != 0: "appId cannot be empty"
        title.length != 0: "title cannot be empty"
        shortTitle.length != 0: "shortTitle cannot be empty"
        description.length != 0: "description cannot be empty"
        level.length != 0: "level cannot be empty"
      }
      self.fantastecId = fantastecId
      self.title = title
      self.shortTitle = shortTitle
      self.description = description
      self.level = level
      
      self.id = FantastecSwap.nextCardCollectionId

      FantastecSwap.nextCardCollectionId =  FantastecSwap.nextCardCollectionId + 1

      // QUESTION: Surely its the resource that emits this event? Stu agrees
      emit CardCollectionCreated(
        id: self.id,
        fantastecId: self.fantastecId,
        appId: self.appId,
        title: self.title,
        shortTitle: self.shortTitle,
        description: self.description,
        level: self.level
      )
    }


    pub fun mintNFT(cardId: UInt64, edition: UInt64): @NFT {
        let card = self.cardsData[cardId]
        let mintedByEdition = card.numberMintedPerEdition[edition]! || 0 // set to zero by default
        let mintNumber = mintedByEdition + 1

        // mint the new item
        let newItem: @NFT <- create NFT(  cardId: cardId,
                                          edition: edition,
                                          mintNumber: mintNumber
                                          )

        // Increment the count of nfts minted for this play
        card.numberMintedPerEdition[edition] = mintNumber

        return <-newItem
    }

    pub fun batchMintNFT(cardId: UInt64, batchMintVolume: UInt64, edition: UInt64): @Collection {
      let newCollection <- create Collection() // check Collection is correct thing to use

      var index: UInt64 = 0
      while index < batchMintVolume {
        index = index + UInt64(1)
        newCollection.deposit(token: <-self.mintNFT(cardId: cardId, edition: edition)
      }

      return <-newCollection
    }

    pub struct NFTData {
      pub let cardId: UInt64
      pub let cardCollectionId: UInt64
      pub let edition: UInt64
      pub let mintNumber: UInt64
      init(cardId: UInt64, cardCollectionId: UInt64, edition: UInt64, mintNumber: UInt64){
        self.cardId = cardId
        self.cardCollectionId = cardCollectionId
        self.edition = edition
        self.mintNumber = mintNumber
      }
    }

    pub resource NFT: NonFungibleToken.INFT {
        pub let data: NFTData

        init( cardId: UInt64,
              cardCollectionId: UInt64,
              edition: UInt64,
              mintNumber: UInt64) {

            // TODO: media stored on NFT for future updating

            // Increment the global IDs
            FantastecSwap.totalSupply = FantastecSwap.totalSupply + 1

            self.id = FantastecSwap.totalSupply

            // set the metadata struct
            self.data = create NFTData(
              cardId: cardId,
              cardCollectionId: cardCollectionId,
              edition: edition,
              mintNumber: mintNumber
            )

            // TODO: do we return trhe entire details for Mongo via this event
            emit NFTMinted(
              id: self.id, 
              cardId: self.data.cardId, 
              cardCollectionId: self.cardCollectionId, 
              edition: self.data.edition, 
              mintNumber: self.data.mintNumber
            )
        }

        destroy() {
            emit NFTDestroyed(id: self.id)
        }
    }


  pub resource CardCollection {
    pub let id: UInt64

    // Array of cards that are part of  this CardCollection
    // When a Card is added to a set it's id is appended
    // An id cannot be removed is the card is locked
    pub var cardIds: [UInt64]
    
    // if card is mintable it's id is set to true, else false
    pub var mintableCardIds: {UInt64: Bool}

    // when locked, cards cannot be added or removed,
    // but minting is still possible
    pub var locked: Bool
    
    // number of cards minted, keyed on card.id
    pub var cardsMinted: {UInt64: UInt64}

    init(
      fantastecId: String, 
      appId: String, 
      title: String, 
      shortTitle: String, 
      description: String, 
      level: String
      ){
      self.id = FantastecSwap.nextCardCollectionId
      self.cardIds  = []
      self.mintableCardIds = {}
      self.locked = false
      self.cardsMinted = {}

      // QUESTION: if FantastecSwap.nextCardCollectionId is used here:
      //    why is it not passed to the struct?
      //    and why does the struct increment nextCardCollectionId?
      // I'd have though struct is a definition only, aka an object,
      //    and this resource would increment contract level vars
      FantastecSwap.swapCollectionsData[self.id] = CardCollectionData(self.fantastecId, self.appId, self.title, self.shortTitle, self.description, self.level)
    }
  }


  //
  // initialisation function
  //
  init() {
    self.cardCollections <- {}
    self.cardCollectionsData = {}
    self.cards <- {}
    self.cardsData  = {}

    self.totalSupply = 0
    self.nextCardId = 1
    self.nextCardCollectionId = 1

    self.account.save<@Admin>(<- create Admin(), to: /storage/FantastecSwapAdmin)

    emit ContractInitialized()
  }
}