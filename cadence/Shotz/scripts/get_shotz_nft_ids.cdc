import ShotzNFT from "../contracts/ShotzNFT.cdc"

pub fun main(address: Address): [UInt64] {
  let nftOwner = getAccount(address)

  let capability = nftOwner.getCapability<&{ShotzNFT.ShotzNFTCollectionPublic}>(/public/ShotzNFTCollection001)

  let receiverRef = capability.borrow()
    ?? panic("Address has no Shotz collection")
  
  return receiverRef.getIDs()
}
