This is a Work in Progress

## file structure

    /cadence - contains current work with cadence contracts

        /FantastecSwap            - Our production contracts
          /FantastecSwapData      - Our data contract
            /scripts              - tests R functions
            /transactions         - tests CUD functions
            /contracts            - the contract

        /standard - contains the standard contract on Flow (FT and NFT at present) for reference purposes

--- anything below this line are prototypes and references
        /KittyItems - the reference implementation provided by DapperLabs

        /Shotz - my last workings out, adapting KittyItems to suite our needs
            /contracts
                /ShotzNFT.cdc - represents an NFT contract proposal
                /ShotzToken.cdc - represents a FT contract proposal
                /scripts - experiments with scripts 
                /transactions - experiments with tranmsactions

## Working with exports from the Playground
You can find the playground here https://play.onflow.org/.  You can export a basic test framework using the Export button.

An implementation of our swap contracts, transactions and script is saved here: https://play.onflow.org/41707531-d33c-4196-80d2-3fc616c8d15b?type=account&id=0

When working with the exports from the playground, before installing with yarn:
1) update your flow-cli with: `brew install flow-cli`
2) update the dependancies in `package.json` manually
3) change all import lines to `import from "flow-js-testing/dist"` not `"flow-js-testing/dist/utils/file"`

Now, install all with yarn

Then yarn init-flow to generate flow.json
Then yarn start-emulator to do just that
In a new console window, jest in the test folder and Bob's your uncle

Q: store card and collection data on contract or in storage? Public or Private?
