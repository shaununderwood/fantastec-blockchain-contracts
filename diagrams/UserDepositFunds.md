# DRAFT: User Deposit Funds
_I haven't check that this is an accurate representation of what happens in our system.  Nor have i checked the Flow behaves in this manner.  Instead, this is a pseudo diagram of fantastec systems and Flow blockchain interactions_


```mermaid
sequenceDiagram
    autonumber
    User->>App: requestToDeposit
    App->>Ramp: requestToDeposit(acctNum, FUSD)
    Ramp->>Ramp: displayDepositOverlay()
    User->>Ramp: amount
    rect rgb(200,250,200)
        Note over Ramp: Moving currencies
        Ramp->>Bank: requestFunds (£)
        Bank->>Ramp: receiveFunds (£)
        Ramp->>Flow: depositFunds (FUSD, acctNum, value)
    end
    rect rgb(200,240,2300)
        Note over Flow: EmitEvent: currency deposited
        Flow-->>Wallet: Event(acctNum, FLOW, value)
    end
    Ramp->>App: success
    App->>Wallet: getBalance()
    Wallet->>App: {vaultBalances}
    App->>User: balance
```
