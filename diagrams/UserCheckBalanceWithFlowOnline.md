# DRAFT: User Checks Balance: Flow **Online**
_I haven't check that this is an accurate representation of what happens in our system.  Nor have i checked the Flow behaves in this manner.  Instead, this is a pseudo diagram of fantastec systems and Flow blockchain interactions_



```mermaid
sequenceDiagram
    autonumber
    User->>App: requestBalance(FUSD)
    App->>Wallet: requestBalance(FUSD)
    Wallet->>Flow: checkVaultBalance(account, FUSD)
    Flow->>Wallet: {vaultBalance}
    Wallet->>DB: updateBalance({vaultBalance})
    Wallet->>App: {vaultBalance}
    App->>User: balance
```
