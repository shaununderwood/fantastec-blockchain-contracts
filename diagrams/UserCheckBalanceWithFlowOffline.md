# DRAFT: User Checks Balance: Flow **Offline**
_I haven't check that this is an accurate representation of what happens in our system.  Nor have i checked the Flow behaves in this manner.  Instead, this is a pseudo diagram of fantastec systems and Flow blockchain interactions_

```mermaid
sequenceDiagram
    autonumber
    User->>App: requestBalance(FUSD)
    App->>Wallet: requestBalance(FUSD)
    Wallet--xFlow: checkVaultBalance
    Note over Flow: Blockchain Offline
    Wallet->>DB: getBalance(account, FUSD)
    DB->>Wallet: {vaultBalance}
    Wallet->>App: {vaultBalance}
    App->>User: balance
    Note over User, App: Blockchain Offline, so may not be accurate
```
