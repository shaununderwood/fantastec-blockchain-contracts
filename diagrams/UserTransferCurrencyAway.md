# DRAFT: User Transfer Currnecy Away
_I haven't check that this is an accurate representation of what happens in our system.  Nor have i checked the Flow behaves in this manner.  Instead, this is a pseudo diagram of fantastec systems and Flow blockchain interactions_



```mermaid
sequenceDiagram
  User->>App: requestTransfer(toAccNum, amount)
  App->>Wallet: requestTransfer(toAccNum, amount)
  Wallet->>Flow: transactionTransfer(fromAccNum, toAccNum, amount)
  rect rgb(200,250,200)
    Note over Flow: Moving currencies
    fromAcc->>Flow: amount
    Flow->>toAcc: amount
    Note over Flow: EmitEvent: Funds Deposited
  end
  Flow->>Wallet: success
  Wallet->>App: success
  App->>User: success
```
