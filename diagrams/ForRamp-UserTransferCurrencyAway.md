# DRAFT: User Transfer Currnecy Away




```mermaid
sequenceDiagram
  UserApp->>FantastecAPI: transfer(toAcc, amount)
  FantastecAPI->>Flow: transfer(fromAcc, toAcc, amount)
  rect rgb(200,250,200)
    Note over Flow: Moving currencies
    fromAcc->>Flow: amount
    Flow->>toAcc: amount
    Note over Flow: EmitEvent: Funds Deposited
  end
```
