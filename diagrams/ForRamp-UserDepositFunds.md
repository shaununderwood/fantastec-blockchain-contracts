# DRAFT: User Deposit Funds

```mermaid
sequenceDiagram
    autonumber
    User->>App: requestToDeposit
    App->>Ramp: requestToDeposit(acctNum, FUSD)
    Ramp->>Ramp: displayDepositOverlay()
    User->>Ramp: amount
    rect rgb(200,250,200)
        Note over Ramp: Moving currencies
        Ramp->>Bank: requestFunds (£)
        Bank->>Ramp: receiveFunds (£)
        Ramp->>Flow: depositFunds (FUSD) (acctNum, value)
    end
    Ramp->>App: success
    App->>Flow: getBalance()
    Flow->>App: {vaultBalances}
    App->>User: balance
```
