# DRAFT: User Registration
_I haven't check that this is an accurate representation of what happens in our system.  Nor have i checked the Flow behaves in this manner.  Instead, this is a pseudo diagram of fantastec systems and Flow blockchain interactions_

Flow may not be available at all times, so Flow account creation needs to be asynchronised by using queue.

```mermaid
sequenceDiagram
    autonumber
    User->>App: register
    App->>Profile: create profile
    Profile->>Wallet: profileId
    Wallet->>Queue: createAccount(profileId)
    loop for each CreateAccount on Queue
        rect rgb(250,200,200)
            Queue->>Flow: createAccount()
            rect rgb(200,250,200)
                Note over Flow: Moving currencies
                Admin Account->>Flow: funds
                Flow->>User Account: funds
            end
          Note over Flow: EmitEvent: Account Creacted
          Flow->>Queue: keys
          Queue->>Queue: saveKeys(keys)
        end
    Flow-->>Profile: account created
    end
    Profile->>App: profile created
    App->>User: homepage
```
