# DRAFT: Event Listener Balance Changed
_I haven't check that this is an accurate representation of what happens in our system.  Nor have i checked the Flow behaves in this manner.  Instead, this is a pseudo diagram of fantastec systems and Flow blockchain interactions_

```mermaid
sequenceDiagram
    autonumber
    rect rgb(200,240,2300)
      Note over Flow: EmitedEvent: currency deposited
      Flow->>Wallet: eventBalance(account, currency, value)
    end
    Wallet->>DB: setBalance(account, currency, value)
```
