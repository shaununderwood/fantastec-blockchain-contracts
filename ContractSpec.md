
# DataContract Specification

__Questions are on their own line and begin with ?__

import: nothing

## Use cases
### Managing Collections
1. When managing collections I want to be able
    1. to add a new collection {collectionId, title, shortTitle, description, level, album, club, marketplaceFeeFix, marketplaceFeePercentage}
    1. to remove a collection {collectionId}
    1. to update a collection's details {collectionId, title, shortTitle, description, level, album, club}
    1. to update a collection's fee attributes {marketplaceFeeFix, marketplaceFeePercentage}
        1. ? could fee attributes be changed at any point in the future? eg we increase the fees on a collection?
        1. ? at this stage do we want to be thinking about Itellectual Property Owner split?
    1. to prevent any further changes to the collection
    1. to prevent any further minting against the collection
      1. ? so if less that 100% of collectibles are minting, the collection blocks any more being minted, they could be said to be burnt off the chain.
    1. to prevent changes happening to a collection once any card in a collection has been minted
        1. ? are there any other properties to add to collection, eg Theme attributes, 
        1. ? are there any fee attributes we may want to change over time? marketplaceFeeFix, marketplaceFeePercentage
        1. ? need to discuss IPO inclusion in the contract and how they are 
    1. Add cards to a collection
    1. Remove card from a collection
    1. Disable a card from being minted
        1. ? might we want to reenable minting?
    1. Manage Intellectual Property Owners (IPO) on collection by
        1. add IPO
        1. CRU IPO percentage

### Managing Cards
1. When managing Cards I want to be able 
    1. to create a new Card {cardId, name, level, aspectRatio, foreground, thumbnailURL, videoURL, mintNumber, collectionId}
    1. to remove a card {cardId}
    1. to update an existing Card {cardId, name, level, aspectRatio, foreground, thumbnailURL, videoURL, mintNumber, collectionId}
    1. to prevent any further changes to the card
    1. to prevent any further minting of the card
    1. to limit the number of NFTs minted against this card
        1. ? this is probably a Collection attribute

### Managing Intellectual Property Owners (IPOs)
1. As an Admin I want to manage Intellectual Property Owners (IPOs) on Flow by
    1. added IPO to the system {id, name, flowAddress}
    1. list all IPOs 
    1. list collections by IPO
    1. get an IPO's fees collected with transaction details
    1. transfer IPO's fees to another account
    1. give access to another Flow Account


### Cards
1. Lookup functions
    1. getCardById(cardId): Card
    1. getCards(): Card[]

### NFT
1. verifyImage(base64ImageData): boolean
1. updateMediaURL(MediaUpdateDetails):boolean



# Minting Contract Specification

import: DataContract, NonFungibleToken

## Use cases
1. As an Admin user I want to be able
    1. to mint X amount of each Card in the collection
        1. ? should cards be mintable individually, or as a whole, or both?
        1. ? do we want to implement a hard upper limit to the number of mintables for a collection, so we can, say, mint portions at any time but never more than a limit?
    1. to be able to set a default account to which all minted items are stored
    1. to be able to specific an account to which a minted NFT is stored
    1. to mint a single specific NFT giving the CardId and mintNumber, as long as
        1. the mintNumber doesn't already exist
        
# Market Contract Specification

## Use Cases

1. calculateFeeBreakdown(collectionId, amount): FeeBreakdown
```
  FeeBreakdown = {
    amount: number,
    collectionId: string,
    totalPayoutValue: number,
    marketplaceFeeValue: number,
    marketplaceFeePercentage: number,
  }
```
2. getListings(): Listing[]
1. getListingsByAddress(flowAddress): Listing[]
1. getListingsByCardId(cardId): Listing[]
1. postListing(collectible, feeBreakdown): listingId
1. closeListing(listingId)
1. purchaseListing(listingId, payerAddress, newOwnerAddress): boolean


# Migration Contract
1. Create UserAccount
1. Deactivate UserAccount
1. Mint NFT(cardId, mintNumber)
1. Deposit NFT to UserAccount
1. List NFTs in UserAccount
1. Add Flow to UserAccount
1. Remove NFT from UserAccount

# Swapping Contract
1. List NFTs in UserAccount
1. In one transaction
    1. Move NFT from UserAccountA to UserAccountB
    1. Move NFT from UserAccountB to UserAccountA
    1. Move Flow between UserAccounts to ensure storage  capacity
    1. Add Flow to UserAccount to ensure storage capacity

# User scripts and transactions
As a User I want to be able to
1. to list NFTs in UserAccount.storage
1. to list FLOW in UserAccount.Vault
1. to list FUSD in UserAccount.Vault
1. to transfer FUSD from UserAccount to UserAccountB


